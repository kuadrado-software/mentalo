run:
	docker-compose up -d

run-log:
	docker-compose up

build:
	docker-compose up --build -d

stop:
	docker-compose down

bash: 
	docker exec -it kuadrado bash

reload:
	docker exec -it kuadrado nginx -s reload

open:
	firefox localhost:8888/

pack:
	./pack-dist.sh

web:
	npm run build

web-prod:
	npm run build-prod