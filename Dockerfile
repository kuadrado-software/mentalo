FROM nginx:alpine

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

WORKDIR /usr/share/nginx/html
COPY . .