#!/usr/bin/env node
const fs = require("fs");
const { bundle } = require("simple-browser-js-bundler");
const _dir = process.cwd();
const build_minified = process.argv.includes("prod");

// Build the root page bundle
bundle(`${_dir}/src/main.js`, `${_dir}/public/js/main.js`, { minify: build_minified });

// Create a separate bundle for each page
const pages = fs.readdirSync(`${_dir}/src/pages`);
for (const page of pages) {

    const stat_page = fs.statSync(`${_dir}/src/pages/${page}`);
    const source_path = `${_dir}/src/pages/${page}/index.js`;

    if (!stat_page.isDirectory()) {
        console.error("Error building page : " + page + " is not a directory");
        continue;
    } else if (!fs.existsSync(source_path)) {
        console.error("Error building page : " + "index.js not found in directory " + page);
        continue;
    }

    const page_bundle_destination_dir = `${_dir}/public/${page}`;

    if (!fs.existsSync(page_bundle_destination_dir)) {
        console.log(`The directory ${page_bundle_destination_dir} doesn't exist ans will be created with an index.html boilerplate.`)
        fs.mkdirSync(page_bundle_destination_dir);

        const page_html = `
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mentalo App - ${page}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
    <meta name="description"
        content="Mentalo app - ${page} page" />
    <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon" />

    <link href="../style/style.css" rel="stylesheet" />
</head>
<body>
    <main></main>
</body>
<script type="text/javascript" src="js/bundle.js"></script>
</html>
            `;

        const page_html_file = fs.createWriteStream(`${page_bundle_destination_dir}/index.html`);
        page_html_file.write(page_html);
    }

    if (!fs.existsSync(`${page_bundle_destination_dir}/js`)) {
        fs.mkdirSync(`${page_bundle_destination_dir}/js`);
    }

    bundle(source_path, `${page_bundle_destination_dir}/js/bundle.js`, { minify: build_minified });
}

// Clean obsolete page directories
function deletePageDirectory(path) {
    try {
        const nestedFiles = fs.readdirSync(path);
        for (const nf of nestedFiles) {
            fs.unlinkSync(`${path}/${nf}`);
        }
        fs.rmdirSync(path);
    } catch (error) {
        console.error(error);
    }
}

const reserved_public_dirs = ["assets", "style", "js", "mockup-data"]; // Ignore the directories that are not pages
const output_pages = fs.readdirSync(`${_dir}/public`).filter(name => {
    const stat = fs.statSync(`${_dir}/public/${name}`);
    return stat.isDirectory() && !reserved_public_dirs.includes(name);
});

for (const page of output_pages) {
    if (!pages.includes(page)) {
        console.log("The public page directory " + page + " will be removed");
        deletePageDirectory(`${_dir}/public/${page}`);
    }
}