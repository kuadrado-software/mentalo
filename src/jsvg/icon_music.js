module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "74.498337mm",
    height: "84.927986mm",
    viewBox: "0 0 74.498337 84.927986",
    version: "1.1",
    class: "svg-icon icon-music",
    contents: [
        {
            tag: "g", transform: "translate(-82.947263,-70.928866)", contents: [
                {
                    tag: "g", contents: [
                        {
                            tag: "path",
                            d: "m 157.4187,70.928866 c -25.04092,11.90625 -34.14313,13.83807 -55.40429,16.29492 v 55.548834 h 4 V 94.470156 c 15.78051,-1.98437 36.1595,-6.57282 47.40429,-13.94336 v 42.968484 h 4 z"
                        },
                        {
                            tag: "ellipse",
                            cx: "5.9403501",
                            cy: "174.02219",
                            rx: "12.473214",
                            ry: "8.3154764",
                            transform: "rotate(-30.902214)"
                        },
                        {
                            tag: "ellipse",
                            cx: "60.206936",
                            cy: "183.54141",
                            rx: "12.473214",
                            ry: "8.3154764",
                            transform: "rotate(-30.902214)"
                        }
                    ]
                },
            ]
        },
    ]
}