module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "42.333332mm",
    height: "42.333332mm",
    viewBox: "0 0 42.333332 42.333332",
    version: "1.1",
    class: "svg-icon icon-plus",
    contents: [
        {
            tag: "g", transform: "translate(12.095243,-29.482144)", contents: [
                {
                    tag: "path",
                    d: "M 6.4467561,53.18881 H -12.095243 V 48.024143 H 6.4467561 V 29.482144 H 11.611423 V 48.024143 H 30.238089 V 53.18881 H 11.611423 V 71.815476 H 6.4467561 Z",
                }
            ]
        },

    ]
}