"use strict";

const ToolTipsManager = require("tooltips-manager");
const translator = require("ks-cheap-translator");
const { app_translations_url } = require("./constants");
const LoaderModal = require("./loader-modal");
const NotifPopup = require("./notif-popup");
const warnNoMobile = require("./warn-no-mobile");


class Loading {
    render() {
        return new LoaderModal().render()
    }
}

const SCREENS = {
    Home: require("./screens/home/home"),
    App: require("./screens/app/app"),
    GamePlayer: require("./screens/game-player"),
    Loading,
};

/**
 * Render the main element at the root of the app.
 */
class RootPage {
    /**
     * Initialize to first screen to a loader, 
     * initialize the translator object and then navigate to the homepage component
     */
    constructor() {
        this.state = {
            screen: { name: "Loading", params: { navigate: this.navigate.bind(this) } }
        };

        this.tooltips_manager = new ToolTipsManager({
            style: { backgroundColor: "#222d", zIndex: 101 },
            pop_tooltip_delay: 200,
        });


        this.init_translator().finally(() => {
            warnNoMobile();
            this.navigate("Home");
        });
    }

    /**
     * Initialize the translator object. Catch the error if wrong locale, default will be used.
     * @returns {Boolean} true
     */
    async init_translator() {
        await translator.init({
            translations_url: app_translations_url,
            supported_languages: ["en", "fr", "es"],
        }).catch(err => {
            console.error(err);
            (new NotifPopup({ error: true, message: "Error: page cannot be translated" })).pop()
        });
        return true;
    }

    /**
     * Handles screen change.
     * @param {String} screen_name The constructor name of the wanted screen
     * @param {Object} params A litteral object that will be passed to the new screen instance as parameter
     */
    navigate(screen_name, params = {}) {
        params = Object.assign(params, { navigate: this.navigate.bind(this) })
        this.state.screen = { name: screen_name, params };
        obj2htm.renderCycle();
    }

    /**
     * Build the rendering object for this component.
     * The screen is rendered as a child node, it's selected by name dynamically from state.
     * If a method clear() exists on the rendered screen instance it will be called.
     * @returns {Object} Object representation of the html element
     */
    render() {
        if (this.register_screen
            && this.register_screen.clear
            && typeof this.register_screen.clear === "function") {
            this.register_screen.clear();
        }

        this.register_screen = new SCREENS[this.state.screen.name](this.state.screen.params);

        return {
            tag: "main",
            contents: [
                this.register_screen.render(),
            ],
        };
    }
}

module.exports = RootPage;
