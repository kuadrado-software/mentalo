"use strict";

/**
 * Self handled notification popup.
 * Can be for either success or error
 */
class NotifPopup {
    /**
     * @param {
     *      close_delay: {Integer}, The time in milliseconds before the popup close itself
     *      message: {String} The string message to display
     *      error: {Boolean} Use error design (red background)
     * } params 
     */
    constructor(params) {
        this.params = params;
        this.close_delay = this.params.close_delay || 2000;
        this.class_name = "mtlo-notif-popup";
        const red = "rgb(223, 65, 3)";
        const green = "rgb(40, 199, 61)";
        this.element = document.createElement("div");
        this.element.innerHTML = this.params.message;
        this.element.id = this.class_name + window.performance.now().toString();
        this.element.classList.add(this.class_name);
        this.element.style.position = "fixed";
        this.element.style.top = "20px";
        this.element.style.right = "30px";
        this.element.style.padding = "20px";
        this.element.style.display = "flex";
        this.element.style.justifyContent = "center";
        this.element.style.alignItems = "center";
        this.element.style.borderRadius = "10px";
        this.element.style.maxWidth = "400px";
        this.element.style.zIndex = 100;
        this.element.style.backgroundColor = this.params.error ? red : green;
        this.element.style.color = "white";
        this.element.style.fontWeight = "bold";
        this.element.style.margin = "5px";

        this.closing_btn = document.createElement("div");
        this.closing_btn.style.position = "absolute";
        this.closing_btn.style.top = 0;
        this.closing_btn.style.right = 0;
        this.closing_btn.style.cursor = "pointer";
        this.closing_btn.style.display = "flex";
        this.closing_btn.style.justifyContent = "center";
        this.closing_btn.style.alignItems = "center";
        this.closing_btn.style.width = "20px";
        this.closing_btn.style.width = "20px";
        this.closing_btn.innerHTML = "x";
        this.closing_btn.style.color = "white";
    }

    /**
     * Show to popup and attach the event listener to handle closing on click the cross
     */
    pop() {
        this.close_timeout = setTimeout(this.close.bind(this), this.close_delay, true);
        this.close = this.close.bind(this);
        this.closing_btn.addEventListener("click", this.close);

        this.element.appendChild(this.closing_btn);
        document.body.appendChild(this.element);
    }

    /**
     * Close the popup and clear timeout and event listener
     * @param {Boolean} at_timeout True if called from timeout closure
     */
    close(at_timeout) {
        this.closing_btn.removeEventListener("click", this.on_close_icon_click);

        if (at_timeout) {
            delete this.close_timeout;
        } else {
            clearTimeout(this.close_timeout);
        }

        this.element.remove();
    }
}

module.exports = NotifPopup;