module.exports = {
    custom_project_mime: "mtl",
    ui_config: {
        project_view: {
            animation_canvas_frame_rate: 30,
            padding: 50
        }
    },
    app_translations_url: "/assets/translations-app/",
    docs_translations_url: "/assets/translations-docs/",
    images_url: "/assets/images/",
    demo_game_assets_url: "/assets/demo_game_assets/",
    import_image_supported_mimes: ["png", "jpg", "bmp", "jpeg"],
    supported_locales: ["fr", "en", "es"]
}