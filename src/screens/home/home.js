"use strict";

const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);
const { custom_project_mime, images_url } = require("../../constants");
const icon_import = require("../../jsvg/icon_import");
const icon_white_sheet = require("../../jsvg/icon_white_sheet");
const { load_project_data } = require("../../project-loader");
const MenuPanel = require("../../menu-panel/menu-panel");
const Project = require("../app/model/project");
const icon_book = require("../../jsvg/icon_book");
const LoaderModal = require("../../loader-modal");
const icon_game_controller = require("../../jsvg/icon_game_controller");
const { fetch_game } = require("../../pages/games/xhr");
const { file_has_ext } = require("../../utils");
const NotifPopup = require("../../notif-popup");

/**
 * The component for the home page, render as default for the root of the app
 */
class Home {
    constructor(params) {
        this.params = params;
        this.menu_panel = new MenuPanel({
            on_changed_language: this.refresh.bind(this),
        });
        this.id = "mtlo-app-home-page";

        this.on_render_ready = this.on_render_ready.bind(this);
        window.addEventListener(obj2htm.event_name, this.on_render_ready);
    }

    /**
     * The callback called when the component has been appended to the DOM as html
     * @param {Event} e 
     */
    on_render_ready(e) {
        if (e.detail.outputNode.querySelector("#" + this.id)) {
            this.handle_load_game_param();
            window.removeEventListener(obj2htm.event_name, this.on_render_ready)
        }
    }

    /**
     * Parses the edit_game param from uri querystring and tries to load the corresponding game
     * The games gets opened in the editor on success.
     */
    async handle_load_game_param() {
        const url_params = new URLSearchParams(window.location.search);
        if (url_params.has("edit_game")) {
            this.show_loader();
            const game_id = url_params.get('edit_game');
            fetch_game(game_id)
                .then(game_data => this.edit_project(game_data))
                .catch(err => {
                    console.log(err);
                    this.refresh();
                });
        }
    }

    /**
     * Open the editor on a new blank project
     */
    start_new_project() {
        this.params.navigate("App", { project: new Project() })
    }

    /**
     * Open the editor with a project instance populated with loaded data
     * @param {Object} project_data A project loaded from JSON data (a file or API response)
     */
    edit_project(project_data) {
        const project = new Project();
        project.load_data(project_data);
        this.params.navigate("App", { project })
    }

    /**
     * Reads a saved project file and open the editor with the loaded project
     * @param {File} file A .mtl file
     */
    load_project(file) {
        this.show_loader();
        load_project_data(file).then(project_data => {
            this.edit_project(project_data);
        }).catch(err => {
            (new NotifPopup({ error: true, message: err })).pop();
            this.refresh();
        });
    }

    /**
     * Refresh the rendering cycle from this node
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
    }

    /**
     * Shows a full window loader
     */
    show_loader() {
        obj2htm.subRender(
            new LoaderModal().render({ message: t("Project is loading") + "..." }),
            document.getElementById(this.id));
    }

    /**
     * Clears the menu sidebar event listeners
     */
    clear() {
        this.menu_panel.clear();
    }

    /**
     * Get the object description of this component
     * @returns {Object} The Object description of the html element for this component
     */
    render() {
        return {
            tag: "div",
            id: this.id,
            contents: [
                this.menu_panel.render(),
                {
                    tag: "div",
                    id: "mtlo-app-home-banner",
                    contents: [
                        {
                            tag: "img", src: `${images_url}logo_mentalo_text.png`,
                            id: "mtlo-home-banner-logo",
                        }
                    ]
                },
                {
                    tag: "div",
                    id: "mtlo-app-home-main-top",
                    contents: [
                        {
                            tag: "div",
                            id: "mtlo-app-home-main-content",
                            contents: [
                                {
                                    tag: "div",
                                    class: "hp-text-block",
                                    contents: [
                                        {
                                            tag: "p",
                                            contents: t("mentalo-about.paragraph")
                                        }
                                    ]
                                },
                                {
                                    tag: "div",
                                    class: "buttons-block",
                                    contents: [
                                        {
                                            tag: "button",
                                            tooltip: t("Start a new game project"),
                                            contents: [
                                                { tag: "span", contents: t("New project"), },
                                                { ...icon_white_sheet },
                                            ],
                                            onclick: this.start_new_project.bind(this)
                                        },
                                        {
                                            tag: "input", type: "file",
                                            id: "load-project-file-input",
                                            style_rules: { display: "none" },
                                            onchange: e => {
                                                const files = e.target.files;
                                                if (files && files[0]) {
                                                    if (!file_has_ext(files[0], custom_project_mime)) {
                                                        (new NotifPopup({
                                                            error: true,
                                                            message: t("Please select a .*** file", {
                                                                ext: custom_project_mime
                                                            })
                                                        })).pop();
                                                        return;
                                                    }
                                                    this.load_project(files[0])
                                                }
                                            }
                                        },
                                        {
                                            tag: "label", htmlFor: "load-project-file-input",
                                            class: "mtlo-app-hp-button",
                                            tooltip: t("Load a game project from a file"),
                                            contents: [
                                                {
                                                    tag: "span",
                                                    contents: t("Load a project"),
                                                },
                                                { ...icon_import }
                                            ]
                                        },
                                        {
                                            tag: "a",
                                            href: "documentation/",
                                            class: "mtlo-app-hp-button",
                                            tooltip: t("Learn how to use the app with the user manual."),
                                            contents: [
                                                {
                                                    tag: "span",
                                                    contents: t("Learn")
                                                },
                                                { ...icon_book }
                                            ]
                                        },
                                        {
                                            tag: "a",
                                            href: "games/",
                                            class: "mtlo-app-hp-button",
                                            tooltip: t("Explore, play and publish games"),
                                            contents: [
                                                {
                                                    tag: "span",
                                                    contents: t("Games")
                                                },
                                                { ...icon_game_controller }
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        {
                            tag: "div",
                            id: "mtlo-app-home-mentaleau-img",
                            contents: [
                                {
                                    tag: "img",
                                    src: `${images_url}mental-eau.png`,
                                }
                            ]
                        }
                    ]
                },

            ]
        }
    }
}

module.exports = Home;