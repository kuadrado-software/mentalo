"use strict";

const { SCENE_TYPES } = require("mentalo-engine");
const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * The component render as the content of the dialbbox opened for the "Scene settings" action
 */
class SceneSettingsInput {
    /**
     * @param {Object} params Required params are project<Project> and scene<Scene>
     */
    constructor(params) {
        this.params = params;
        this.id = "scene-settings-input";
        this.ui_state = {
            select_scene_type_value: this.params.scene._type,
            show_warn: false,
            show_destination_select: !this.params.scene.end_cinematic_options.quit,
        };
    }

    /**
     * Handles select input change for the type of the scene
     * @param {Event} e 
     */
    handle_select_type_change(e) {
        const value = e.target.value;
        this.ui_state.show_warn = value === SCENE_TYPES.CINEMATIC;
        this.ui_state.select_scene_type_value = value;
        this.refresh();
    }

    /**
     * Handle change from select input for the destination scene set in case the scene is a cinematic
     * @param {Event} e 
     */
    handle_change_end_action(e) {
        this.ui_state.show_destination_select = e.target.id === "end-cinematic-action-input-change-scene";
        obj2htm.subRender(
            this.render_end_action_options(),
            document.getElementById("cinematic-end-action-options"),
            { mode: "replace" })
    }

    /**
     * @returns {Object} The object representation of the html to render for the destination scene select input
     */
    render_destination_scene_select() {
        const { project, scene } = this.params;
        const select_scenes = project.scenes.filter(s => s !== scene);
        return {
            tag: "select",
            id: "cinematic-destination-scene-input",
            class: "center-y",
            contents: [{
                tag: "option", contents: "-- " + t("Choose the next scene") + " --", value: -1,
            }].concat(select_scenes.length === 0 ? [
                {
                    tag: "option",
                    value: "",
                    disabled: true,
                    contents: t("There is no scenes available. Create some and set up this later."),
                    class: "empty-select-placeholder"
                }
            ] : select_scenes.map(s => {
                const index = project.scenes.indexOf(s);
                return {
                    tag: "option", contents: s.name, value: index,
                    selected: scene.end_cinematic_options.destination_scene_index === index,
                }
            })),
        }
    }

    /**
     * @returns {Object} The object representation of the html to render for the end of cinematic options
     */
    render_end_action_options() {
        const { show_destination_select } = this.ui_state;
        return {
            tag: "li", id: "cinematic-end-action-options",
            contents: [
                {
                    tag: "label",
                    class: "center-y",
                    contents: t("End of cinematic action"),
                    tooltip: t("Set up the automatic action to perform at the end of the cinematic"),
                    style_rules: { width: "200px" }
                },
                {
                    tag: "ul",
                    class: "list-settings",
                    contents: [
                        {
                            tag: "li",
                            contents: [
                                {
                                    tag: "label", contents: t("Go to the next scene"), class: "center-y",
                                    htmlFor: "end-cinematic-action-input-change-scene",
                                    style_rules: { cursor: "pointer" },
                                },
                                {
                                    tag: "input", type: "radio", name: "end-cinematic-action",
                                    id: "end-cinematic-action-input-change-scene",
                                    onchange: this.handle_change_end_action.bind(this),
                                    checked: show_destination_select,
                                    class: "center-y",
                                },
                                show_destination_select && this.render_destination_scene_select(),
                            ]
                        },
                        {
                            tag: "li",
                            contents: [
                                {
                                    tag: "label", contents: t("End of program"), class: "center-y",
                                    htmlFor: "end-cinematic-action-input-quit",
                                    style_rules: { cursor: "pointer" },
                                },
                                {
                                    tag: "input", type: "radio", name: "end-cinematic-action",
                                    id: "end-cinematic-action-input-quit",
                                    onchange: this.handle_change_end_action.bind(this),
                                    checked: !show_destination_select,
                                    class: "center-y"
                                },
                            ]
                        }
                    ]
                }
            ]
        }
    }

    /**
     * Refreshes the rendering tree from this component
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" })
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        const { scene } = this.params;
        const scene_types = Object.values(SCENE_TYPES).map(type => {
            return {
                text: t(type),
                value: type,
                tip: (function () {
                    switch (type) {
                        case SCENE_TYPES.CINEMATIC:
                            return t("A cinematic scene is just an image or an animation to watch for the player, there is no choices, no inventory and no objects.")
                        case SCENE_TYPES.PLAYABLE:
                            return t("A playable scene has a set of possible actions for the player with the choices, the objects and the inventory.")
                    }
                })()
            }
        });
        return {
            tag: "ul", id: this.id,
            class: "list-settings",
            contents: [
                {
                    tag: "li",
                    contents: [
                        {
                            tag: "label", contents: t("Scene type"), class: "center-y",
                            style_rules: { width: "200px" }
                        },
                        {
                            tag: "select",
                            id: "select-scene-type-input",
                            class: "center-y",
                            onchange: this.handle_select_type_change.bind(this),
                            contents: Object.values(scene_types).map(type => {
                                return {
                                    tag: "option",
                                    title: type.tip,
                                    value: type.value,
                                    contents: type.text,
                                    selected: this.ui_state.select_scene_type_value === type.value,
                                }
                            }),
                        },
                        this.ui_state.show_warn && {
                            tag: "span", class: "warn-message text-s center-y",
                            contents: t("Objects, choices and text box will be deleted.")
                        },
                    ]
                },
                this.ui_state.select_scene_type_value === SCENE_TYPES.CINEMATIC && {
                    tag: "li",
                    tooltip: t("Cinematic duration in seconds"),
                    contents: [
                        {
                            tag: "label", contents: t("Cinematic duration"),
                            class: "center-y",
                            style_rules: { width: "200px" }
                        },
                        {
                            tag: "input", type: "number", min: "0", step: 1,
                            class: "center-y",
                            style_rules: { width: "50px" },
                            value: scene.cinematic_duration, id: "scene-cinematic-duration-input"
                        },
                        {
                            tag: "span",
                            class: "text-s center-y",
                            contents: t("For a duration 0, the cinematic will stop at the end of the last frame.")
                        }
                    ]
                },
                this.ui_state.select_scene_type_value === SCENE_TYPES.CINEMATIC && this.render_end_action_options(),
            ],
        }
    }
}

module.exports = SceneSettingsInput;