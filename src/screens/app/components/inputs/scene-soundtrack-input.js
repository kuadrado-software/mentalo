"use strict";

const { MtlSoundTrack } = require("mentalo-engine");
const translator = require("ks-cheap-translator");
const icon_import = require("../../../../jsvg/icon_import");
const icon_non_empty_folder = require("../../../../jsvg/icon_non_empty_folder");
const t = translator.trad.bind(translator);

/**
 * The component being rendered as the content of the dial box opened for the "Scene soundtrack" action
 */
class SceneSoundtrackInput {
    constructor(params) {
        this.params = params;
        this.state = {
            player_preview: {
                id: "scene-soundtrack-input-player-preview",
                track: new MtlSoundTrack({ resources_index: this.params.resources_index }),
            },
        };

        const { current_track } = this.params;

        if (current_track.initialized) {
            this.init_track({
                name: current_track.name,
                _loop: current_track.audio.loop,
            });
        }
    }

    /**
     * Load some litteral data to initialize the instance
     * @param {Object} data 
     */
    init_track(data) {
        const { name, _loop } = data;

        this.state.player_preview.track.init({
            name,
            _loop,
        });

        this.state.player_preview.track.on_load(() => {
            this.refresh_player_preview();
        });
    }

    /**
     * Creates and return the html element displaying the soundtrack player
     * @returns {HtmlElement}
     */
    get_player_preview() {
        const fig = document.createElement("figure");
        fig.classList.add("soundtrack-player-figure");
        const caption = document.createElement("figcaption");
        caption.innerHTML = this.state.player_preview.track.name;
        fig.appendChild(caption);
        const audio = this.state.player_preview.track.audio;
        audio.controls = true;
        fig.appendChild(audio);
        return fig;
    }

    /**
     * Handles change from the file input and calls init_track() with the new file data
     * @param {Event} e 
     */
    handle_file_input_change(e) {
        const file = (e.target.files || [{ name: "New soundtrack", select_empty: true }])[0];

        const name_input = document.getElementById("scene-soundtrack-name-input");
        if (name_input) {
            name_input.value = file.name;
        }

        document.getElementById("selected-file-tip").innerHTML = t("Selected file") + " : " + file.name;

        if (file && !file.select_empty) {
            const f_reader = new FileReader();
            f_reader.onload = () => {

                this.params.resources_index.add_sound({
                    name: file.name,
                    src: f_reader.result,
                });

                this.init_track({
                    name: file.name,
                    _loop: false,
                });

                this.refresh_base_settings_list();
                this.refresh_player_preview();
            };
            f_reader.readAsDataURL(file);
        }
    }

    /**
     * Refreshes the rendering of the base settings list
     */
    refresh_base_settings_list() {
        obj2htm.subRender(
            this.render_base_settings_list(),
            document.getElementById("scene-soundtrack-input-settings-list"),
            { mode: "replace" }
        );
    }

    /**
     * @returns {Object} thie object representation of the html to render for the base settings list
     */
    render_base_settings_list() {
        const { current_track } = this.params;
        const current_track_is_set = current_track.initialized && !current_track.empty;

        const some_file_selected = function () {
            const input = document.getElementById("scene-soundtrack-file-input");
            return input && input.files && input.files[0] ? input.files[0] : undefined
        }();

        return {
            tag: "ul",
            id: "scene-soundtrack-input-settings-list",
            class: "list-settings",
            contents: some_file_selected || current_track_is_set ? [
                {
                    tag: "li", contents: [
                        {
                            tag: "label", contents: t("Track's name"),
                            style_rules: { width: "200px" }, class: "center-y",
                        },
                        {
                            tag: "input", type: "text",
                            id: "scene-soundtrack-name-input",
                            value: some_file_selected ? some_file_selected.name : current_track.name,
                            required: true,
                            style_rules: { flex: 1 },
                            class: "center-y",
                        },
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "label", contents: t("Play as loop"),
                            style_rules: { width: "200px" }, class: "center-y",
                        },
                        {
                            tag: "input", type: "checkbox",
                            id: "scene-soundtrack-loop-input",
                            checked: current_track.audio.loop,
                            class: "center-y",
                        }
                    ]
                },
            ] : []
        }
    }

    /**
     * Refreshes the rendering of the audio player
     */
    refresh_player_preview() {
        document.getElementById(this.state.player_preview.id).innerHTML = "";
        document.getElementById(this.state.player_preview.id).appendChild(this.get_player_preview());
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const { current_track } = this.params;
        return {
            tag: "div",
            contents: [
                {
                    tag: "div", id: this.state.player_preview.id,
                },
                {
                    tag: "div", class: "action-buttons-row", contents: [
                        {
                            tag: "label",
                            class: "action-button",
                            htmlFor: "scene-soundtrack-file-input",
                            tooltip: t("Import a new track from my computer"),
                            contents: [{ ...icon_import }]
                        },

                        {
                            tag: "input",
                            id: "scene-soundtrack-file-input",
                            style_rules: { display: "none" },
                            type: "file",
                            accept: "audio/*",
                            required: !current_track.initialized,
                            onchange: this.handle_file_input_change.bind(this)
                        },
                        this.params.on_choose_soundtrack_from_existing && {
                            tag: "button",
                            tooltip: t("Choose from the soundtracks already imported"),
                            onclick: this.params.on_choose_soundtrack_from_existing,
                            class: "action-button",
                            contents: [{ ...icon_non_empty_folder }],
                        },
                    ]
                },
                {
                    tag: "div", id: "selected-file-tip", contents: "", class: "text-green text-s",
                },
                this.render_base_settings_list(),
            ]
        }
    }
}

module.exports = SceneSoundtrackInput;