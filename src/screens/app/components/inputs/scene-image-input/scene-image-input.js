"use strict";

const { ui_config, import_image_supported_mimes } = require("../../../../../constants");
const translator = require("ks-cheap-translator");
const icon_import = require("../../../../../jsvg/icon_import");
const icon_new_drawing = require("../../../../../jsvg/icon_new_drawing");
const icon_edit_drawing = require("../../../../../jsvg/icon_edit_drawing");
const { mentalo_drawing_tool_editable_file_mime } = require("mentalo-drawing-tool");
const icon_non_empty_folder = require("../../../../../jsvg/icon_non_empty_folder");
const t = translator.trad.bind(translator);

/**
 * The component displayed as the content of the dialbox for the action of setting up the scene image
 */
class SceneImageInput {
    constructor(params) {
        this.params = params;
        const { current_anim } = this.params;

        this.current_state = { ...current_anim };
    }

    /**
     * Handle changes from file input
     * @param {Event} e 
     */
    handle_change_image_file_input(e) {
        const file = (e.target.files || [{ name: "New animation" }])[0];
        const image_name_input = document.getElementById("scene-image-name-input");
        if (image_name_input) {
            image_name_input.value = file.name;
        }
        document.getElementById("selected-file-tip").innerHTML = t("Selected file") + " : " + file.name;
        this.refresh_base_settings_list();
    }

    /**
     * Handle changes from number input and sets current_state frame number
     * @param {Event} e 
     */
    handle_change_frame_nb(e) {
        const { current_state } = this;
        current_state.frame_nb = parseInt(e.target.value);

        obj2htm.subRender(
            this.render_animation_related_inputs(),
            document.getElementById("animation-related-inputs"),
            { mode: "replace" });
    }

    /**
     * Handle changes from range inputs and sets current_state frame rate
     * @param {Event} e 
     */
    handle_change_animation_speed(e) {
        const { current_state } = this;

        current_state.speed = parseInt(e.target.value);
        obj2htm.subRender(
            this.render_fps_tip(),
            document.getElementById("scene-image-fps-tip"),
            { mode: "replace" });
    }

    /**
     * Handle animation frame rate change from increment/decrement buttons
     * @param {Number} inc 1 or -1
     * @param {Number} max max frame rate value 
     */
    handle_change_animation_speed_inc(inc, max) {
        const { current_state } = this;

        current_state.speed = Math.min(max, Math.max(1, current_state.speed + inc));

        obj2htm.subRender(
            this.render_animation_related_inputs(),
            document.getElementById("animation-related-inputs"),
            { mode: "replace" });
    }

    /**
     * @returns {Object} The object representation of the html to render for the frame rate input
     */
    render_speed_input() {
        const { current_state } = this;

        return {
            tag: "li", contents: [
                {
                    tag: "label", contents: t("Frame rate"),
                    class: "center-y",
                    style_rules: { width: "200px" }
                },
                {
                    tag: "div",
                    class: "center-y",
                    style_rules: {
                        display: "flex",
                        gap: "5px",
                        alignItems: "center"
                    }, contents: [
                        { tag: "i", contents: t("faster") },
                        {
                            tag: "input", type: "range", min: 1, max: 60, step: 1, id: "scene-anim-speed-input",
                            style_rules: { flex: 1 },
                            value: current_state.speed,
                            oninput: this.handle_change_animation_speed.bind(this),
                        },
                        { tag: "i", contents: t("slower") },
                        {
                            tag: "div",
                            style_rules: {
                                display: "grid",
                                alignItems: "center",
                                gridTemplateColumns: "1fr 1fr",
                                gap: "3px",
                                paddingLeft: "10px",
                            },
                            contents: [{ inc: -1, txt: "-" }, { inc: 1, txt: "+" }].map(button => {
                                return {
                                    tag: "button",
                                    style_rules: {
                                        height: "30px",
                                        width: "30px",
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center",
                                    },
                                    class: "action-text-button",
                                    contents: button.txt,
                                    onclick: this.handle_change_animation_speed_inc.bind(this, button.inc, 60),
                                }
                            })
                        },
                        this.render_fps_tip(),
                    ]
                },

            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the inputs displayed if the image has more than 1 frame
     */
    render_animation_related_inputs() {
        const { current_state } = this;

        return {
            tag: "ul",
            class: "list-settings",
            id: "animation-related-inputs",
            style_rules: {
                display: current_state.frame_nb > 1 ? "block" : "none",
            },
            contents: [
                current_state.frame_nb > 1 && this.render_speed_input(),
                current_state.frame_nb > 1 && {
                    tag: "li", contents: [
                        {
                            tag: "label",
                            contents: t("Play as loop"),
                            style_rules: { width: "200px" },
                            class: "center-y",
                        },
                        {
                            tag: "input", type: "checkbox",
                            checked: !current_state.play_once,
                            id: "scene-anim-loop-input",
                            class: "center-y"
                        },
                    ]
                }
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the span 
     * that indicates the currently set animation speed in frame per second
     */
    render_fps_tip() {
        const { current_state } = this;
        const frame_rate = ui_config.project_view.animation_canvas_frame_rate
        const current_speed = frame_rate / current_state.speed;
        return {
            tag: "b", id: "scene-image-fps-tip",
            class: "text-green",
            style_rules: {
                width: "100px",
                marginLeft: "15px"
            },
            contents: `${current_speed.toFixed(1).toString().replace(".0", "")} fps`,
        }

    }

    /**
     * @returns {Object} the object representation of the html to render for the animation name
     */
    render_current_anim_name() {
        const { current_state } = this;
        return {
            tag: "div", contents: [
                { tag: "span", contents: t("Current image") + " : " + current_state.name },
            ],
            style_rules: {
                display: current_state.initialized ? "block" : "none",
                maxWidth: "400px",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis"
            }
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the basic settings list
     */
    render_base_settings_list() {
        const { current_state } = this;
        const current_state_is_set = current_state.initialized && !current_state.empty;

        const some_file_selected = function () {
            const f_input = document.getElementById("scene-image-file-input");
            return f_input && f_input.files && f_input.files[0] ? f_input.files[0] : undefined
        }();

        return {
            tag: "ul",
            id: "scene-image-input-settings-list",
            class: "list-settings",
            contents: current_state_is_set || some_file_selected ? [
                {
                    tag: "li", contents: [
                        {
                            tag: "label", contents: t("Image's name"), class: "center-y",
                            style_rules: { width: "200px" },
                        },
                        {
                            tag: "input", type: "text",
                            style_rules: { flex: 1 },
                            class: "center-y",
                            id: "scene-image-name-input",
                            value: some_file_selected ? some_file_selected.name : current_state.name,
                            required: true,
                        },
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "label", contents: t("Frame number"), class: "center-y",
                            style_rules: { width: "200px" },
                        },
                        {
                            tag: "input", type: "number", min: 1, step: 1,
                            id: "scene-anim-frame-nb-input",
                            style_rules: { width: "50px" },
                            class: "center-y",
                            value: current_state.frame_nb,
                            onchange: this.handle_change_frame_nb.bind(this),
                        }
                    ]
                },
            ] : []
        };
    }

    refresh_base_settings_list() {
        obj2htm.subRender(
            this.render_base_settings_list(),
            document.getElementById("scene-image-input-settings-list"),
            { mode: "replace" }
        );
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const { current_state } = this;
        return {
            tag: "div", id: "scene-image-input",
            contents: [
                this.render_current_anim_name(),
                {
                    tag: "div",
                    class: "action-buttons-row",
                    contents: [
                        current_state.initialized && {
                            tag: "button",
                            class: "action-button",
                            contents: [{ ...icon_edit_drawing }],
                            tooltip: t("Edit the current image"),
                            onclick: this.params.on_edit_image
                        },
                        {
                            tag: "button",
                            tooltip: t("Create a new image"),
                            onclick: this.params.on_create_image,
                            class: "action-button",
                            contents: [{ ...icon_new_drawing }],
                        },
                        this.params.on_choose_image_from_existing && {
                            tag: "button",
                            tooltip: t("Choose from the images already imported"),
                            onclick: this.params.on_choose_image_from_existing,
                            class: "action-button",
                            contents: [{ ...icon_non_empty_folder }],
                        },
                        {
                            tag: "label",
                            class: "action-button",
                            htmlFor: "scene-image-file-input",
                            tooltip: t("Import a new image from my computer"),
                            contents: [{ ...icon_import }]
                        },
                        {
                            tag: "input",
                            id: "scene-image-file-input",
                            style_rules: { display: "none" },
                            type: "file",
                            accept: `${import_image_supported_mimes
                                .concat(mentalo_drawing_tool_editable_file_mime)
                                .map(ext => '.' + ext)
                                .join(',')}`,
                            required: !current_state.initialized,
                            onchange: this.handle_change_image_file_input.bind(this)
                        },
                    ]
                },
                {
                    tag: "div", id: "selected-file-tip", contents: "", class: "text-green text-s",
                },
                this.render_base_settings_list(),
                this.render_animation_related_inputs(),
            ]
        }
    }
}

module.exports = SceneImageInput;