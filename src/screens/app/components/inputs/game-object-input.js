"use strict";

const { mentalo_drawing_tool_editable_file_mime } = require("mentalo-drawing-tool");
const translator = require("ks-cheap-translator");
const { import_image_supported_mimes } = require("../../../../constants");
const icon_edit_drawing = require("../../../../jsvg/icon_edit_drawing");
const icon_import = require("../../../../jsvg/icon_import");
const icon_new_drawing = require("../../../../jsvg/icon_new_drawing");
const icon_non_empty_folder = require("../../../../jsvg/icon_non_empty_folder");
const t = translator.trad.bind(translator);

/**
 * The component rendered as the content of the dialbox opened for the Add object or Edit object action
 */
class GameObjectInput {
    /**
     * 
     * @param {Object} params Required params are on_create_image<Function> 
     * and on_edit_image<Function> is required only if edit_object<GameObject> is present
     */
    constructor(params = {}) {
        this.params = params;
    }

    /**
     * Handles change event from file input (image file)
     * Refreshes the displayed file name directly in the DOM.
     * @param {Event} e 
     */
    handle_file_input_change(e) {
        const file = (e.target.files || [{ name: "New object" }])[0];
        document.getElementById("selected-file-tip").innerHTML = t("Selected file") + " : " + file.name;
        this.refresh_settings_list();
    }

    /**
     * Refreshes the rendering of the settings list
     */
    refresh_settings_list() {
        obj2htm.subRender(
            this.render_settings_list(),
            document.getElementById("game-object-input-settings-list"),
            { mode: "replace" }
        );
    }

    /**
     * @returns {Object} The object representation of the html to render fot the settings list
     */
    render_settings_list() {
        const { edit_object } = this.params;

        const some_file_selected = function () {
            const input = document.getElementById("game-object-file-input");
            return input && input.files && input.files[0] ? input.files[0] : undefined;
        }();

        return {
            tag: "ul",
            class: "list-settings",
            id: "game-object-input-settings-list",
            contents: !edit_object && !some_file_selected ? [] : [
                {
                    tag: "li", contents: [
                        {
                            tag: "label", contents: t("Object's name"), class: "center-y",
                            style_rules: { width: "200px" },
                        },
                        {
                            tag: "input", type: "text",
                            style_rules: { flex: 1 },
                            class: "center-y",
                            id: "game-object-name-input",
                            value: edit_object
                                ? edit_object.name
                                : "",
                            required: true,
                        },
                    ]
                },
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const { edit_object } = this.params;
        return {
            tag: "div", id: "game-object-input", contents: [
                {
                    tag: "div",
                    class: "action-buttons-row",
                    contents: [
                        edit_object && {
                            tag: "button",
                            class: "action-button",
                            contents: [{ ...icon_edit_drawing }],
                            tooltip: t("Edit the current image"),
                            onclick: this.params.on_edit_image
                        },
                        {
                            tag: "button",
                            tooltip: t("Create a new image"),
                            onclick: this.params.on_create_image,
                            class: "action-button",
                            contents: [{ ...icon_new_drawing }],
                        },
                        this.params.on_choose_image_from_existing && {
                            tag: "button",
                            tooltip: t("Choose from the images already imported"),
                            onclick: this.params.on_choose_image_from_existing,
                            class: "action-button",
                            contents: [{ ...icon_non_empty_folder }],
                        },
                        {
                            tag: "label",
                            class: "action-button",
                            htmlFor: "game-object-file-input",
                            tooltip: t("Import a new image from my computer"),
                            contents: [{ ...icon_import }]
                        }, ,
                        {
                            tag: "input", type: "file",
                            accept: `${import_image_supported_mimes
                                .concat(mentalo_drawing_tool_editable_file_mime)
                                .map(ext => '.' + ext)
                                .join(',')}`,
                            id: "game-object-file-input",
                            style_rules: { display: "none" },
                            required: !edit_object,
                            onchange: this.handle_file_input_change.bind(this)
                        },
                    ]
                },
                {
                    tag: "div", id: "selected-file-tip", contents: "", class: "text-green text-s",
                },
                this.render_settings_list(),
            ]
        }
    }
}

module.exports = GameObjectInput;