"use strict";

const { SCENE_TYPES } = require("mentalo-engine");
const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * The component for the choices panel displayed below the image of the edited scene.
 */
class SceneChoices {
    /**
     * @param {Object} params Required parameters are project<Project> and on_edit_choice<Function>
     */
    constructor(params) {
        this.params = params;
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        const { project, on_edit_choice } = this.params;
        const edit_scene = project.get_scene();
        const ui_settings = project.game_ui_options.choices_panel;

        return {
            tag: "div",
            id: "choices-container",
            class: "canvas-container",
            contents: [
                {
                    tag: "div",
                    class: "canvas-like",
                    id: "choices-canvas",
                    style_rules: {
                        backgroundColor: ui_settings.background_color,
                        padding: `${ui_settings.container_padding}px`,
                        visibility: edit_scene._type === SCENE_TYPES.PLAYABLE ? "visible" : "hidden",
                    },
                    contents: edit_scene.choices.map(choice => {
                        return {
                            tag: "div", contents: choice.text,
                            onclick: () => on_edit_choice(choice),
                            onmouseenter: e => {
                                e.target.style.backgroundColor = ui_settings.active_choice_background_color;
                                e.target.style.border = `${ui_settings.active_choice_border_width}px solid ${ui_settings.font_color}`;
                            },
                            onmouseleave: e => {
                                e.target.style.backgroundColor = "unset";
                                e.target.style.border = `${ui_settings.active_choice_border_width}px solid #0000`;
                            },
                            style_rules: {
                                cursor: "pointer",
                                fontSize: `${ui_settings.font_size}px`,
                                fontFamily: ui_settings.font_family,
                                textAlign: ui_settings.text_align,
                                fontWeight: ui_settings.font_weight,
                                fontStyle: ui_settings.font_style,
                                color: ui_settings.font_color,
                                padding: `${ui_settings.choice_padding}px ${ui_settings.choice_padding}px`,
                                position: "relative",
                                display: "flex",
                                justifyContent: ui_settings.text_align,
                                borderRadius: `${ui_settings.active_choice_rounded_corners_radius}px`,
                                border: `${ui_settings.active_choice_border_width}px solid #0000`,
                            },
                        }
                    }),
                }
            ]
        }
    }
}

module.exports = SceneChoices;