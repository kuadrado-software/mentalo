"use strict";

const translator = require("ks-cheap-translator");
const { custom_project_mime, images_url } = require("../../../../constants");
const icon_exit = require("../../../../jsvg/icon_exit");
const icon_export = require("../../../../jsvg/icon_export");
const icon_import = require("../../../../jsvg/icon_import");
const icon_play = require("../../../../jsvg/icon_play");
const icon_plus = require("../../../../jsvg/icon_plus");
const NotifPopup = require("../../../../notif-popup");
const { load_project_data } = require("../../../../project-loader");
const { file_has_ext } = require("../../../../utils");
const UiActions = require("../ui-actions/ui-actions");
const t = translator.trad.bind(translator);

/**
 * This is the component displayed as the left sidebar in the editor,
 * It contains buttons and forms for all the possible actions at project level (not at scene level)
 */
class ActionPanel {
    constructor(params) {
        this.params = params;

        this.init_control_s_listener();
    }

    /**
     * Registers the event listeners to handle the Ctrl+s keyboard shortcut
     */
    init_control_s_listener() {
        this.ctrl_s_state = {
            ctrl_press: false,
        };

        this.ctrl_s_listeners = {
            ctrl: {
                down: e => {
                    if (e.key === "Control") {
                        this.ctrl_s_state.ctrl_press = true;
                    }
                },
                up: e => {
                    if (e.key === "Control") {
                        this.ctrl_s_state.ctrl_press = false;
                    }
                }
            },
            s: e => {
                if (e.key.toLowerCase() === 's' && this.ctrl_s_state.ctrl_press) {
                    // Keep the prevent default even if ctrl_s is disabled by a child component 
                    // because default will be to save the html page and it can be confusing in any case in this context.
                    e.preventDefault();
                    if (this.params.ctrl_s_is_enabled()) {
                        this.ctrl_s_state.ctrl_press = false;
                        this.handle_export_project();
                    }
                }
            }
        };
        window.addEventListener("keydown", this.ctrl_s_listeners.ctrl.down);
        window.addEventListener("keyup", this.ctrl_s_listeners.ctrl.up);
        window.addEventListener("keydown", this.ctrl_s_listeners.s);
    }

    /**
     * Removes the event listeners for Ctrl+s
     */
    clear() {
        window.removeEventListener("keydown", this.ctrl_s_listeners.ctrl.down);
        window.removeEventListener("keyup", this.ctrl_s_listeners.ctrl.up);
        window.removeEventListener("keydown", this.ctrl_s_listeners.s);
    }

    /**
     * Creates a Blob with the project data and triggers the prompt to download the file.
     * @param {Object} options 
     */
    handle_export_project(options = { exit: false, load_data: undefined }) {
        const project_blob = new Blob([this.params.project.serialized()], { type: "application/json" });
        const url = URL.createObjectURL(project_blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = `${this.params.project.name.replace(/\s+/g, "-")}.${custom_project_mime}`;
        link.click();
        URL.revokeObjectURL(url);
        this.params.on_save_project(options);
    }

    /**
     * Handles a file input change and loads a project data from the selected file.
     * @param {Event} e 
     */
    handle_import_project(e) {
        const files = e.target.files;
        if (files && files[0]) {
            if (!file_has_ext(files[0], custom_project_mime)) {
                (new NotifPopup({
                    error: true,
                    message: t(
                        "Please select a .*** file",
                        { ext: custom_project_mime })
                })).pop();
            } else {
                this.params.show_loading_state({ message: t("Project is loading") + "..." });
                load_project_data(files[0])
                    .then(project_data => {
                        this.params.hide_loading_state();
                        this.params.on_load_project_data(project_data);
                    })
                    .catch(err => {
                        this.params.hide_loading_state();
                        (new NotifPopup({ error: true, message: err })).pop();
                    });
            }

        }
    }

    /**
     * Adds a new scene to the project and select it as the currently edited scene.
     */
    handle_add_scene() {
        const { project, project_view, set_unsaved } = this.params;
        const new_scene = project.add_scene();
        project.select_scene(new_scene);
        project_view.refresh();
        this.refresh_scenes_actions();
        set_unsaved();
    }

    /**
     * Handles change from scene select input
     * @param {Event} e 
     */
    handle_select_scene(e) {
        const { project, project_view } = this.params;
        project.state.scene = e.target.value;
        this.refresh_scenes_actions();
        project_view.refresh();
    }

    /**
     * Handle change from starting point scene select input
     * @param {Event} e 
     */
    handle_change_starting_point(e) {
        const { project, set_unsaved } = this.params;
        project.starting_scene_index = parseInt(e.target.value);
        set_unsaved();
    }

    /**
     * Refreshes the rendering tree from this component.
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById("project-action-panel"), { mode: "replace" });
    }

    /**
     * Refreshes the rendering of the "scenes" section of the panel
     */
    refresh_scenes_actions() {
        obj2htm.subRender(
            this.render_scenes_actions(),
            document.getElementById("action-panel-scenes-actions"),
            { mode: "replace" });
    }

    /**
     * @returns {Object} The object representation of the html component for the Project section of the actions
     */
    render_project_actions() {
        return {
            tag: "div",
            class: "action-block",
            contents: [
                { tag: "h3", contents: t("Project"), class: "action-block-title" },
                {
                    tag: "input", type: "file",
                    id: "import-project-file-input",
                    style_rules: { display: "none" },
                    onchange: this.handle_import_project.bind(this)
                },
                {
                    tag: "ul", class: "action-block-actions", contents: [
                        {
                            tag: "li", contents: [
                                {
                                    tag: "label",
                                    class: "action-button",
                                    tooltip: t("Import a project"),
                                    htmlFor: "import-project-file-input",
                                    contents: [
                                        { ...icon_import },
                                    ]
                                }
                            ],
                        },
                    ].concat([
                        {
                            text: t("Export the project to a file"),
                            onclick: this.handle_export_project.bind(this),
                            icon: icon_export,
                        },
                        {
                            text: t("Test play the game"),
                            onclick: this.params.test_play_project,
                            icon: icon_play,
                        },
                        {
                            text: t("Quit"),
                            onclick: this.params.on_exit_project,
                            icon: icon_exit,
                        }
                    ].filter(action => action.cond ? action.cond() : true)
                        .map(action => {
                            return {
                                tag: "li",
                                contents: [
                                    {
                                        tag: "button",
                                        contents: [{ ...action.icon }],
                                        onclick: action.onclick,
                                        class: "action-button",
                                        tooltip: action.text,
                                    }
                                ]

                            }
                        })
                    ),
                },
            ],
        }
    }

    /**
     * @returns {Object} The object representation of the component containing the actions related to the scenes
     */
    render_scenes_actions() {
        const { project } = this.params;
        return {
            tag: "div",
            class: "action-block",
            id: "action-panel-scenes-actions",
            contents: [
                { tag: "h3", contents: t("Scenes"), class: "action-block-title" },
                {
                    tag: "ul",
                    class: "action-block-actions",
                    contents: [
                        {
                            tag: "li", contents: [
                                {
                                    tag: "div", class: "action-select-block",
                                    tooltip: t("Choose the scene you want to edit"),
                                    contents: [
                                        { tag: "label", contents: t("Edit a scene") },
                                        {
                                            tag: "select", contents: project.scenes.map(scene => {
                                                return {
                                                    tag: "option",
                                                    value: project.scenes.indexOf(scene),
                                                    selected: project.get_scene() === scene,
                                                    contents: scene.name,
                                                }
                                            }),
                                            onchange: this.handle_select_scene.bind(this)
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            tag: "li", contents: [
                                {
                                    tag: "div", class: "action-select-block",
                                    tooltip: t("Choose the scene you want to start the game with."),
                                    contents: [
                                        { tag: "label", contents: t("Starting point") },
                                        {
                                            tag: "select", contents: project.scenes.map(scene => {
                                                const value = project.scenes.indexOf(scene);
                                                return {
                                                    tag: "option",
                                                    value,
                                                    selected: project.starting_scene_index === value,
                                                    contents: scene.name,
                                                }
                                            }),
                                            onchange: this.handle_change_starting_point.bind(this)
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            tag: "li", contents: [
                                {
                                    tag: "button", contents: [
                                        { ...icon_plus }
                                    ],
                                    tooltip: t("Create a new scene"),
                                    class: "action-button",
                                    onclick: this.handle_add_scene.bind(this)
                                }
                            ]
                        },
                    ]
                }
            ]
        }
    }

    /**
     * @returns {Object} The object representation of the html to render for the UiActions component
     */
    render_ui_actions() {
        return new UiActions({
            project: this.params.project,
            project_view: this.params.project_view,
            set_unsaved: this.params.set_unsaved,
        }).render();
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        return {
            tag: "aside",
            id: "project-action-panel",
            contents: [
                {
                    tag: "div",
                    class: "mentalo-logo-banner",
                    contents: [
                        { tag: "img", src: `${images_url}logo_mentalo_text.png` }
                    ]
                },
                this.render_project_actions(),
                this.render_scenes_actions(),
                this.render_ui_actions(),
            ],
        }
    }
}

module.exports = ActionPanel;