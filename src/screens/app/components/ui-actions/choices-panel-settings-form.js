"use strict";

const TextSettingsInputs = require("../inputs/text-settings-inputs/text-settings-inputs");
const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * The component for the styling settings of the game choices panel
 */
class ChoicesPanelSettingsForm {
    constructor(params) {
        this.params = params;
    }

    /**
     * Handles an input change and modifies the project.game_ui_options.choices_panel object
     * @param {String} property The key name of the property being modified
     * @param {Any} value A string or a number as the value of the property
     */
    handle_change_property(property, value) {
        value = typeof value !== "number" && !isNaN(parseInt(value))
            ? value.includes('.')
                ? parseFloat(value)
                : parseInt(value)
            : value;

        this.params.project.game_ui_options.choices_panel[property] = value;
        this.params.on_change_setting();
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        const current_settings = this.params.project.game_ui_options.choices_panel;
        return {
            tag: "ul", class: "ui-settings-form",
            contents: [
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Text") }
                            ]
                        },
                        new TextSettingsInputs({
                            current_settings,
                            on_change_property: this.handle_change_property.bind(this),
                        }).render(),
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Panel") }
                            ]
                        },
                        {
                            tag: "ul", class: "settings-list", contents: [
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Background color") },
                                        {
                                            tag: "input", type: "color", value: current_settings.background_color,
                                            onchange: e => this.handle_change_property("background_color", e.target.value)
                                        },
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Padding") },
                                        {
                                            tag: "input", type: "range", step: 1, min: 0, max: 40, value: current_settings.container_padding,
                                            onchange: e => this.handle_change_property("container_padding", e.target.value)
                                        },
                                    ]
                                },
                            ]
                        },

                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Choices") }
                            ]
                        },
                        {
                            tag: "ul", class: "settings-list", contents: [
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Padding") },
                                        {
                                            tag: "input", type: "range", step: 1, min: 0, max: 40, value: current_settings.choice_padding,
                                            onchange: e => this.handle_change_property("choice_padding", e.target.value)
                                        },
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Hovered choice background") },
                                        {
                                            tag: "select", onchange: e => this.handle_change_property("active_choice_background_color", e.target.value),
                                            contents: [
                                                {
                                                    tag: "option", value: "rgba(0,0,0,.4)", contents: t("Darken"),
                                                    selected: current_settings.active_choice_background_color === "rgba(0,0,0,.4)",
                                                },
                                                {
                                                    tag: "option", value: "rgba(255,255,255,.4)", contents: t("Lighten"),
                                                    selected: current_settings.active_choice_background_color === "rgba(255,255,255,.4)",
                                                },
                                                {
                                                    tag: "option", value: "rgba(0,0,0,0)", contents: t("No effect"),
                                                    selected: current_settings.active_choice_background_color === "rgba(0,0,0,0)",
                                                },
                                            ]
                                        },
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Hovered choice rounded corners") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 30, step: 1,
                                            value: current_settings.active_choice_rounded_corners_radius,
                                            onchange: e => this.handle_change_property("active_choice_rounded_corners_radius", parseInt(e.target.value))
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Hovered choice border width") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 10, step: 1,
                                            value: current_settings.active_choice_border_width,
                                            onchange: e => this.handle_change_property("active_choice_border_width", parseInt(e.target.value))
                                        }
                                    ]
                                },
                            ]
                        },

                    ]
                },
            ]
        }
    }
}

module.exports = ChoicesPanelSettingsForm;