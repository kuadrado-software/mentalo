"use strict";

const translator = require("ks-cheap-translator");
const icon_choices = require("../../../../jsvg/icon_choices");
const icon_cube = require("../../../../jsvg/icon_cube");
const icon_dialog = require("../../../../jsvg/icon_dialog");
const icon_inventory = require("../../../../jsvg/icon_inventory");
const ChoicesPanelSettingsForm = require("./choices-panel-settings-form");
const GeneralSettingsForm = require("./general-settings-form");
const InventorySettingsForm = require("./inventory-settings-form");
const TextBoxSettingsForm = require("./text-box-settings-form");

const t = translator.trad.bind(translator);

/**
 * The component that displays the list of settings form and action buttons available
 * at project level in the settings section of the sidebar.
 */
class UiActions {
    constructor(params) {
        this.params = params;

        // Define an index of actions with individual states that will be used in render
        this.actions = [
            {
                icon: icon_cube,
                show_form: false,
                render_form: this.render_general_settings.bind(this),
                tip: t("General settings"),
            },
            {
                icon: icon_dialog,
                show_form: false,
                render_form: this.render_text_boxes_settings.bind(this),
                tip: t("Text boxes settings"),
            },
            {
                icon: icon_choices,
                show_form: false,
                render_form: this.render_choices_panel_settings.bind(this),
                tip: t("Choices panel settings"),
            },
            {
                icon: icon_inventory,
                show_form: false,
                render_form: this.render_inventory_settings.bind(this),
                tip: t("Inventory panel settings")
            },
        ];
    }

    /**
     * Handles click on the element rendered from one of the actions described in the index this.actions.
     * @param {Object} action {icon, show_form, render_form, tip}
     */
    handle_click_action(action) {
        if (action.show_form) {
            action.show_form = false;
        } else {
            action.show_form = true;
            this.actions.filter(a => a !== action).forEach(a => { a.show_form = false });
        }
        this.refresh();
    }

    /**
     * Callback called from children components when a setting is updated
     */
    on_change_setting() {
        this.refresh();
        this.params.project_view.refresh();
        this.params.set_unsaved();
    }

    /**
     * Refresh the rendering of the whole component
     */
    refresh() {
        obj2htm.subRender(this.render(),
            document.getElementById("ui-actions-component"),
            { mode: "replace" });
    }

    /**
     * @returns {Object} the object representation of the html to render for the GeneralSettingsForm component
     */
    render_general_settings() {
        return new GeneralSettingsForm({
            project: this.params.project,
            project_view: this.params.project_view,
            on_change_setting: this.on_change_setting.bind(this),
        }).render();
    }

    /**
     * @returns {Object} the object representation of the html to render for the TextBoxSettingsForm component
     */
    render_text_boxes_settings() {
        return new TextBoxSettingsForm({
            project: this.params.project,
            on_change_setting: this.on_change_setting.bind(this),
        }).render();
    }

    /**
     * @returns {Object} the object representation of the html to render for the ChoicesPanelSettingsForm component
     */
    render_choices_panel_settings() {
        return new ChoicesPanelSettingsForm({
            project: this.params.project,
            project_view: this.params.project_view,
            on_change_setting: this.on_change_setting.bind(this),
        }).render();
    }

    /**
    * @returns {Object} the object representation of the html to render for the InventorySettingsForm component
    */
    render_inventory_settings() {
        return new InventorySettingsForm({
            project: this.params.project,
            project_view: this.params.project_view,
            on_change_setting: this.on_change_setting.bind(this),
        }).render();
    }

    /**
    * @returns {Object} the object representation of the html to render for this component
    */
    render() {
        return {
            tag: "div",
            class: "action-block",
            id: "ui-actions-component", contents: [
                { tag: "h3", contents: t("Settings"), class: "action-block-title" },
                {
                    tag: "ul",
                    class: "action-block-actions",
                    contents: this.actions.map(action => {
                        return {
                            tag: "li", contents: [
                                {
                                    tag: "button", contents: [{ ...action.icon }],
                                    tooltip: action.tip,
                                    class: "action-button-wide " + (action.show_form ? "active" : ""),
                                    onclick: this.handle_click_action.bind(this, action)
                                }
                            ]
                        };
                    }),
                },
                (() => {
                    const show_form = this.actions.find(a => a.show_form);
                    return show_form ? show_form.render_form() : undefined
                })()
            ]
        }
    }
}

module.exports = UiActions;