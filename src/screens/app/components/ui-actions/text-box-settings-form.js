"use strict";

const TextSettingsInputs = require("../inputs/text-settings-inputs/text-settings-inputs");
const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * The component that displays the settings form for the text boxes styling in the side toolbar.
 */
class TextBoxSettingsForm {
    constructor(params) {
        this.params = params;
    }

    /**
     * Handles changes from input for any property
     * @param {String} property 
     * @param {Any} value 
     */
    handle_change_property(property, value) {
        value = !isNaN(parseInt(value))
            ? parseInt(value)
            : value;

        this.params.project.game_ui_options.text_boxes[property] = value;
        this.params.on_change_setting();
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const current_settings = this.params.project.game_ui_options.text_boxes;
        return {
            tag: "ul", class: "ui-settings-form",
            contents: [
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Text") }
                            ]
                        },
                        new TextSettingsInputs({
                            on_change_property: this.handle_change_property.bind(this),
                            current_settings,
                        }).render()
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Box") }
                            ]
                        },
                        {
                            tag: "ul", class: "settings-list", contents: [
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Background color") },
                                        {
                                            tag: "input", type: "color", value: current_settings.background_color,
                                            onchange: e => this.handle_change_property("background_color", e.target.value)
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Transparency") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 255,
                                            value: parseInt(current_settings.background_color.slice(7) || "ff", 16),
                                            onchange: e => {
                                                const hex_alpha = `0${parseInt(e.target.value).toString(16)}`.slice(-2);
                                                const new_value = current_settings.background_color.slice(0, 7) + hex_alpha;
                                                this.handle_change_property("background_color", new_value);
                                            }
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Rounded corners") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 30, step: 1,
                                            value: current_settings.rounded_corners_radius,
                                            onchange: e => this.handle_change_property("rounded_corners_radius", e.target.value)
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Border width") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 10, step: 1,
                                            value: current_settings.border_width,
                                            onchange: e => this.handle_change_property("border_width", e.target.value)
                                        }
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ]
        }
    }
}

module.exports = TextBoxSettingsForm;