"use strict";

const LoaderModal = require("../../loader-modal");
const MenuPanel = require("../../menu-panel/menu-panel");
const ActionPanel = require("./components/action-panel/action-panel");
const DialBox = require("./components/dial-box/dial-box");
const ProjectView = require("./components/project-view/project-view");

/**
 * The Mentalo core application, to create and edit games.
 */
class App {
    constructor(params) {
        this.params = params;

        this.state = params.app_state || {
            saved: true,
            ctrl_s_enabled: true
        };

        this.menu_panel = new MenuPanel({
            on_changed_language: this.refresh.bind(this)
        });

        this.id = "app-screen";

        this.clean_window_url();
    }

    /**
     * Removes search parameters that could be in the window location url because we don't want them 
     * to be indefinitely reused if the users tries to exit the app screen.
     */
    clean_window_url() {

        window.history.replaceState(
            null, "", window.location.pathname
        );
    }

    /**
     * Will be called as callback when project is saved to a file
     * @param {Object} options 
     */
    on_save_project(options = { exit: false }) {
        this.state.saved = true;
        if (options.exit) {
            this.on_exit_project();
        } else if (options.load_data) {
            this.on_load_project_data(options.load_data);
        }
    }

    /**
     * This is called on any change made to the project data.
     */
    set_unsaved() {
        this.state.saved = false;
    }

    /**
     * Can be called from a child component that needs to deactivate or reactivate the ctrl+s keyboard shortcut
     * (Used when openning the drawing tool because it is confusing to save the project while an applet is open on top of it)
     * @param {Boolean} value
     */
    disable_ctrl_s(value = true) {
        this.state.ctrl_s_enabled = !value
    }

    /**
     * Opens a dialog box with some parameters, depending of the content of the dialog box.
     * @param {Object} params 
     */
    show_dialbox(params) {
        if (!this.state.register_dialbox) {
            this.state.register_dialbox = new DialBox(params);
            obj2htm.subRender(this.render_dialbox(), document.getElementById("project-view"), { mode: "append" });
        } else {
            this.state.register_dialbox.clear_errors();
            this.state.register_dialbox.params = params;
            this.state.register_dialbox.refresh();
        }
    }

    /**
     * Closes the dialog box, clean up event listeners
     */
    hide_dialbox() {
        this.state.register_dialbox.remove_cancel_listeners();
        this.kill_dialbox();
        obj2htm.subRender({ tag: "div" }, document.getElementById("dial-box-modal"), { mode: "remove" })
    }

    /**
     * Deletes the registered dialbox instance if it exists
     */
    kill_dialbox() {
        if (this.state.register_dialbox) {
            delete this.state.register_dialbox;
        }
    }

    /**
     * Get the render object of the dialbox
     * @returns {Object} the object representation of the html for the dialbox component
     */
    render_dialbox() {
        return {
            tag: "div",
            id: "dial-box-modal",
            contents: [this.state.register_dialbox.render()]
        };
    }

    /**
     * Refreshes the rendering of the action_panel component
     */
    refresh_action_panel() {
        this.action_panel.refresh();
    }

    /**
     * Navigates to the game player screen with the edited project data
     */
    test_play_project() {
        this.action_panel.clear(); // clear ctrl+s listeners
        this.params.navigate("GamePlayer", {
            project: this.params.project,
            from_app: true,
            app_state: this.state
        });
    }

    /**
     * A callback called when the editor gets closed.
     * Handles the state.saved flag.
     * @param {Object} options 
     */
    on_exit_project(options = { ignore_unsaved: false, save_and_exit: false }) {
        if (this.state.saved || options.ignore_unsaved) {
            this.action_panel.clear(); // clear ctrl+s listeners
            this.params.navigate("Home");
        } else if (options.save_and_exit) {
            this.action_panel.handle_export_project({ exit: true });
        } else {
            this.project_view.on_exit_unsaved_project(this.on_exit_project.bind(this));
        }
    }

    /**
     * Callback closed when a new file is loaded into the editor
     * @param {Object} data A deserialized project
     * @param {Object} options 
     */
    on_load_project_data(data, options = { ignore_unsaved: false, save_and_exit: false }) {
        if (this.state.saved || options.ignore_unsaved) {
            this.params.project.load_data(data);
            this.project_view.refresh()
            this.action_panel.refresh();
        } else if (options.save_and_exit) {
            this.action_panel.handle_export_project({ load_data: data });
        } else {
            this.project_view.on_exit_unsaved_project(this.on_load_project_data.bind(this, data));
        }

    }

    /**
     * Refreshes the rendering on individual children components project_view, action_panl and menu_panel
     */
    refresh() {
        this.project_view.refresh();
        this.action_panel.refresh();
        this.menu_panel.refresh();
    }

    /**
     * Shows a full window loader with the needed options like message etc.
     * @param {Object} options 
     */
    show_loading_state(options) {
        obj2htm.subRender(new LoaderModal().render(options), document.getElementById(this.id));
    }

    /**
     * Closes the loader modal
     */
    hide_loading_state() {
        const modal = document.getElementsByClassName("mtlo-loader-modal")[0];
        modal && modal.remove();
    }

    /**
     * @returns {Object} Object description of the html element for this component
     */
    render() {
        const { project } = this.params;

        this.project_view = new ProjectView({
            project,
            show_dialbox: this.show_dialbox.bind(this),
            hide_dialbox: this.hide_dialbox.bind(this),
            kill_dialbox: this.kill_dialbox.bind(this),
            refresh_action_panel: this.refresh_action_panel.bind(this),
            set_unsaved: this.set_unsaved.bind(this),
            disable_ctrl_s: this.disable_ctrl_s.bind(this),
        });

        this.action_panel = new ActionPanel({
            project,
            project_view: this.project_view,
            test_play_project: this.test_play_project.bind(this),
            on_save_project: this.on_save_project.bind(this),
            on_exit_project: this.on_exit_project.bind(this),
            set_unsaved: this.set_unsaved.bind(this),
            on_load_project_data: this.on_load_project_data.bind(this),
            show_loading_state: this.show_loading_state.bind(this),
            hide_loading_state: this.hide_loading_state.bind(this),
            ctrl_s_is_enabled: () => this.state.ctrl_s_enabled
        });

        return {
            tag: "div",
            id: this.id,
            contents: [
                this.menu_panel.render(),
                this.action_panel.render(),
                this.project_view.render(),
                {
                    tag: "a",
                    id: "export-project-link",
                    style_rules: { display: "none" },
                },
            ],
        }
    }
}

module.exports = App