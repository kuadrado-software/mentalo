"use strict";

const { MtlGame } = require("mentalo-engine");
const Scene = require("./scene");
const translator = require("ks-cheap-translator");
const ResourcesIndex = require("./resources_index");
const t = translator.trad.bind(translator);

/**
 * The type used to represent the editable format of a Game instance
 */
class Project extends MtlGame {
    constructor() {
        super();

        this.name = t("New project");
        this.resources_index = new ResourcesIndex();
        this.scenes = [new Scene({ resources_index: this.resources_index })];

        this.game_ui_options.choices_panel.max_choices = 4;
    }

    /**
     * Select a scene to edit
     * @param {Scene} scene
     */
    select_scene(scene) {
        this.state.scene = this.scenes.indexOf(scene);
    }

    /**
     * Removes a scene from the project
     * @param {Scene} scene 
     */
    delete_scene(scene) {
        if (this.scenes.length === 1) {
            this.scenes = [new Scene({ resources_index: this.resources_index })];
        } else {
            const index = this.scenes.indexOf(scene);
            this.scenes.splice(index, 1);
            const new_edit_index = index === 0 ? 0 : index - 1;
            this.state.scene = new_edit_index;
            if (this.starting_scene_index === index) {
                this.starting_scene_index = Math.max(0, this.starting_scene_index - 1)
            }
        }
    }

    /**
     * Adds a new Scene to the project scenes field and returns it
     * @returns {Scene}
     */
    add_scene() {
        this.scenes.push(new Scene({ resources_index: this.resources_index }));
        const index = this.scenes.length - 1;
        this.scenes[index].name = `${t("Scene")} ${index + 1}`;
        return this.scenes[index];
    }

    /**
     * Returns a concatenation of all game objects in the game.
     * @returns {Array<GameObject>}
     */
    get_game_objects() {
        return this.scenes.reduce((acc, scene) => acc.concat(scene.game_objects), []);
    }

    /**
     * Loads a litteral version of a project and populates the instance with the data
     * @param {Object} data
     */
    load_data(data) {
        super.load_data(data);
        this.game_ui_options.choices_panel.max_choices = 4; // This is constant and it's not saved in the exported files

        this.scenes = data.scenes.map(scene => {
            const scene_instance = new Scene({ resources_index: this.resources_index });
            scene_instance.load_data(scene);
            return scene_instance;
        });
    }

    /**
     * @returns {JSON} The instance serialized
     */
    serialized() {
        return JSON.stringify(this.as_litteral());
    }

    /**
     * @returns {Object} The instance formatted as a litteral object
     */
    as_litteral() {
        return {
            name: this.name,
            scenes: this.scenes.map(s => s.as_litteral()),
            game_ui_options: this.game_ui_options,
            starting_scene_index: this.starting_scene_index,
            resources_index: this.resources_index.as_litteral(),
        }
    }
}

module.exports = Project;