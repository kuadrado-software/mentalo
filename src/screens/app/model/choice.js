"use strict";

const { MtlChoice } = require("mentalo-engine");

/**
 * The type for the field choices of a Scene instance.
 */
class Choice extends MtlChoice {
    /**
     * @returns {Object} The instance formatted as a litteral object that can be serialized safely
     */
    as_litteral() {
        const { text, destination_scene_index, use_objects } = this;
        return {
            text, destination_scene_index, use_objects
        }
    }

    /**
     * @returns The serialized instance
     */
    serialized() {
        return JSON.stringify(this.as_litteral());
    }
}

module.exports = Choice;