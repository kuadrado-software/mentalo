"use strict";

const { MentaloEngine } = require("mentalo-engine");
const { ui_config } = require("../constants");
const translator = require("ks-cheap-translator");

/**
 * A component that holds the canvas created by the game engine
 */
class GamePlayer {
    constructor(params) {
        this.params = params;

        this.watch_and_start_engine();
    }

    /**
     * Attached a listener to start the engine as soon as this component has been rendered into the DOM
     */
    watch_and_start_engine() {
        /**
         * Calls this.start() when the component rendering is ready
         * @param {Event} e 
         */
        this.watch_and_start_listener = e => {
            if (e.detail.outputNode.querySelector("#mentalo-game-player-root")) {
                window.removeEventListener(obj2htm.event_name, this.watch_and_start_listener);
                this.start();
            }
        };

        window.addEventListener(obj2htm.event_name, this.watch_and_start_listener);
    }

    /**
     * Will be called as a callback when game is closed.
     */
    on_quit_game() {
        if (this.params.from_app) {
            this.params.navigate("App", {
                project: this.params.project,
                app_state: this.params.app_state
            });
        } else {
            this.params.navigate("Home");
        }
    }

    /**
     * Created a new MentaloEngine instance with the currently edited project as game data
     * Init the engine and runs the game.
     */
    start() {
        this.engine = new MentaloEngine({
            game_data: this.params.project.as_litteral(),
            container: document.getElementById("mentalo-game-player-root"),
            fullscreen: true,
            frame_rate: ui_config.project_view.animation_canvas_frame_rate,
            on_quit_game: this.on_quit_game.bind(this),
            use_locale: translator.locale
        });

        this.engine.init();
        this.engine.run_game();
    }

    /**
     * @returns {Object} The object description of the html to render
     */
    render() {
        return {
            tag: "div",
            id: "mentalo-game-player-root",
            style_rules: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "100vw",
                height: "100vh",
                overflow: "hidden",
            }
        }
    }
}

module.exports = GamePlayer;