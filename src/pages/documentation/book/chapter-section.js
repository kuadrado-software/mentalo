const ImageStepsCarousel = require("./image-steps-carousel");
const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);


/**
 * The component that displays a section of a chapter
 */
class ChapterSection {
    /**
     * 
     * @param {Object} params Required params are
     *      chapter_slug<String> The slug of the chapter parent of this section
     *      title<String> The title of the section
     *      slug<String> The slug of the section
     * Optional
     *      image<String> The uri of the image to display in this section
     *      paragraph_params<Object> If the paragraph of this section requires some dynamic parameters 
     *                              to be inserted in the translation, they must be passed here
     */
    constructor(params) {
        this.chapter_slug = params.chapter_slug;
        this.title = params.title;
        this.slug = params.slug;
        this.image_steps = params.image_steps || [];
        this.paragraph_params = params.paragraph_params || {};
    }

    /**
     * @returns {Object} the object representation of the html to render the book page corresponding the the content of this section
     */
    render_page() {
        return {
            tag: "div", contents: [
                { tag: "h2", contents: t(this.title), class: "chapter-section-title" },
                {
                    tag: "div",
                    class: "chapter-section-page-contents",
                    contents: [
                        {
                            tag: "p",
                            contents: t(`chapter:${this.chapter_slug}.section:${this.slug}.paragraph`, this.paragraph_params),
                            class: "chapter-section-paragraph"
                        },
                        this.image_steps.length > 0 && new ImageStepsCarousel({ image_steps: this.image_steps }).render()
                    ]
                }

            ]
        }
    }
}

module.exports = ChapterSection;