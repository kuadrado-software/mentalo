"use strict";

const translator = require("ks-cheap-translator");
const { images_url } = require("../../../constants");
const t = translator.trad.bind(translator);
const ChapterSection = require("./chapter-section");

/**
 * A component that displays a chapter home contents and a summary of the sections
 */
class Chapter {
    constructor(params) {
        this.title = params.title;
        this.slug = params.slug;
        this.image = params.image;
        this.sections = params.sections.map(s => new ChapterSection(Object.assign(s, { chapter_slug: this.slug })));
    }

    /**
     * @param {Function} on_summary_item_click 
     * @returns {Object} the object representation of the html to render for the Chapter home page
     */
    render_page(on_summary_item_click) {
        return {
            tag: "div", contents: [
                { tag: "h2", contents: t(this.title), class: "chapter-title" },
                {
                    tag: "div",
                    class: "chapter-page-contents",
                    contents: [
                        { tag: "p", contents: t(`chapter:${this.slug}.paragraph`), class: "chapter-paragraph" },
                        this.image && { tag: "img", src: `${images_url}${this.image}`, class: "chapter-image" },
                        this.sections.length > 0 && {
                            tag: "div",
                            class: "chapter-summary",
                            contents: [
                                { tag: "h3", contents: t("Summary") },
                                {
                                    tag: "ul",
                                    contents: this.sections.map((s, i) => {
                                        return {
                                            tag: "li", contents: t(s.title),
                                            onclick: () => on_summary_item_click(this, i)
                                        }
                                    }),
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }
}

module.exports = Chapter;