"use strict";
const { images_url } = require("../../../constants");

/**
 * The component that displays the images of a section in a carousel
 */
class ImageStepsCarousel {
    /**
     * @param {Object} params required param image_steps: {Array<String>}
     */
    constructor(params) {
        this.image_steps = params.image_steps;
        this.state = {
            show_step: 0,
            thumbnails_scroll_x: 0,
        };

        this.run();
    }

    /**
     * Cancels the carousel automation
     */
    clear() {
        clearInterval(window.carousel_autorun_id);
    }

    /**
     * Updates the index of the image to show.
     * @param {Integer} index 
     * @param {Boolean} manual True if the method is called from a manual click on a thumbnail
     */
    show_step(index, manual = false) {
        if (manual) this.clear();

        this.state.show_step = index;
        this.refresh();
    }

    /**
     * Starts the carousel automation.
     */
    run() {
        if (window.carousel_autorun_id) {
            this.clear();
        }

        window.carousel_autorun_id = setInterval(() => {
            this.show_step(this.state.show_step === this.image_steps.length - 1 ? 0 : this.state.show_step + 1)
        }, 2000);
    }

    /**
     * Callback called when the images of the thumnails row are rendered.
     * If the image is selected and out of vision, the thumnails row is scrolled to get the image back in visibility.
     * @param {HTMLElement} img_node 
     */
    handle_selected_thumbnail_scroll_correction(img_node) {
        if (img_node.classList.contains("selected")) {
            const img_bounds = img_node.getBoundingClientRect();
            const parent_bounds = img_node.parentNode.getBoundingClientRect();
            const dif_right = img_bounds.right > parent_bounds.right;
            const dif_left = img_bounds.left < parent_bounds.left;
            if (dif_left || dif_right) {
                const thumbnails = img_node.parentNode;
                const correction = dif_right ? img_bounds.right - parent_bounds.right : img_bounds.left - parent_bounds.left;
                this.state.thumbnails_scroll_x = thumbnails.scrollLeft + correction;
                thumbnails.scrollTo(this.state.thumbnails_scroll_x, 0);
            }
        }
    }


    /**
     * Refreshes the rendering of the component
     */
    refresh() {
        const node = document.getElementById("chapter-image-steps-carousel");
        if (!node) {
            this.clear();
        } else {
            obj2htm.subRender(this.render(), node, { mode: "replace" });
        }
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        const { image_steps } = this;
        const { show_step } = this.state;
        const show_image = image_steps[show_step];

        return {
            tag: "div",
            id: "chapter-image-steps-carousel",
            class: "chapter-image-steps-container",
            contents: [
                {
                    tag: "div",
                    class: "steps-carousel",
                    contents: [{
                        tag: "img", src: `${images_url}${show_image}`
                    }]
                },
                image_steps.length > 1 && {
                    tag: "div",
                    class: "steps-carousel-thumbnails",
                    on_render: node => {
                        node.scrollTo(this.state.thumbnails_scroll_x, 0);
                    },
                    onscroll: e => this.state.thumbnails_scroll_x = e.target.scrollLeft,
                    contents: image_steps.map((image, i) => {
                        return {
                            tag: "img",
                            class: i === show_step ? "selected" : "",
                            src: `${images_url}${image}`,
                            onclick: this.show_step.bind(this, i, true),
                            on_render: this.handle_selected_thumbnail_scroll_correction.bind(this)
                        }
                    }),
                }
            ]
        }
    }
}

module.exports = ImageStepsCarousel;