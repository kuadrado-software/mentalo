"use strict";
const { demo_game_assets_url } = require("../../../constants");
const Chapter = require("./chapter");

/**
 * The index of the chapters and sections of the documentation book
 */
module.exports = {
    chapters: [
        new Chapter({
            title: "Introduction",
            slug: "intro",
            image: "mental-eau-no-stirrer.png",
            sections: [
                {
                    title: "What is Mentalo?",
                    slug: "what-is-mentalo"
                },
                {
                    title: "Kuadrado Software",
                    slug: "kuadrado-software",
                }
            ]
        }),
        new Chapter({
            title: "Mentalo games",
            slug: "mentalo-games",
            image: "compil_screenshots_old_games.png",
            sections: [
                {
                    title: "How to play",
                    slug: "how-to-play",
                },
                {
                    title: "Structure of a game made with Mentalo",
                    slug: "structure-of-a-game-made-with-mentalo",
                    image_steps: ["screen_demo_mentalo_game.png"]
                }
            ]
        }),
        new Chapter({
            title: "Building a game step by step",
            slug: "building-a-game-step-by-step",
            image: "screen_mentalo_new_project.png",
            sections: [
                {
                    title: "The game logic",
                    slug: "game-logic",
                },
                {
                    title: "Interface customization",
                    slug: "interface-customization",
                    image_steps: ["screen_general_settings.png"]
                },
                {
                    title: "Image of the scene",
                    slug: "image-of-the-scene",
                    image_steps: [
                        "screen_add_scene_image_1.png",
                        "screen_add_scene_image_2.png",
                        "screen_add_scene_image_3.png",
                        "screen_add_scene_image_4.png",
                    ],
                    paragraph_params: {
                        chalet_image_link: `${demo_game_assets_url}home_sweet_home/images/chalet.png`
                    },
                },
                {
                    title: "Set the titles",
                    slug: "titles",
                    image_steps: ["screen_edit_title.png"],
                },
                {
                    title: "Add a text box to the scene",
                    slug: "add-text-box-to-scene",
                    image_steps: [
                        "screen_add_text_box_1.png",
                        "screen_add_text_box_2.png",
                        "screen_add_text_box_3.png",
                        "screen_settings_text_boxes_1.png",
                        "screen_settings_text_boxes_2.png"
                    ],
                },
                {
                    title: "Soundtrack of the scene",
                    slug: "soundtrack-of-the-scene",
                    image_steps: [
                        "screen_add_sound_1.png",
                        "screen_add_sound_2.png",
                        "screen_add_sound_3.png",
                    ],
                    paragraph_params: {
                        chalet_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/chalet.mp3`,
                        forest_1_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/forest_1.mp3`,
                        forest_2_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/forest_2.mp3`,
                        forest_3_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/forest_3.mp3`,
                        forest_4_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/forest_4.mp3`,
                        fire_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/fire.mp3`,
                        rain_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/rain.mp3`,
                        brico_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/brico.mp3`,
                        cabin_sound_link: `${demo_game_assets_url}home_sweet_home/sounds/cabin.mp3`,
                    }
                },
                {
                    title: "Create the choices of the scene",
                    slug: "create-the-choices-of-the-scene",
                    image_steps: ["screen_add_choice.png"],
                },
                {
                    title: "Create new scenes",
                    slug: "create-new-scenes",
                    image_steps: [
                        "screen_add_scenes_1.png",
                        "screen_add_scenes_2.png",
                        "screen_add_scenes_3.png",
                        "screen_add_scenes_4.png",
                        "screen_add_scenes_5.png",
                        "screen_add_scenes_6.png",
                    ],
                    paragraph_params: {
                        forest_1_image_link: `${demo_game_assets_url}home_sweet_home/images/forest_1.png`,
                        forest_2_image_link: `${demo_game_assets_url}home_sweet_home/images/forest_2.png`,
                        forest_3_image_link: `${demo_game_assets_url}home_sweet_home/images/forest_3.png`,
                        forest_4_image_link: `${demo_game_assets_url}home_sweet_home/images/forest_4.png`,
                    },
                },
                {
                    title: "Add objects to the scene",
                    slug: "add-objects",
                    image_steps: [
                        "screen_add_object_1.png",
                        "screen_add_object_2.png",
                        "screen_add_object_3.png",
                        "screen_add_object_4.png",
                        "screen_add_object_5.png",
                        "screen_add_object_6.png",
                    ],
                    paragraph_params: {
                        key_image_link: `${demo_game_assets_url}home_sweet_home/images/key.png`
                    }
                },
                {
                    title: "Create the cinematics",
                    slug: "cinematics",
                    image_steps: [
                        "screen_create_cinematics_1.png",
                        "screen_create_cinematics_2.png",
                        "screen_create_cinematics_3.png",
                        "screen_create_cinematics_4.png",
                        "screen_create_cinematics_5.png",
                        "screen_create_cinematics_6.png",
                        "screen_create_cinematics_7.png",
                    ],
                    paragraph_params: {
                        cinematic_inside_chalet_image_link: `${demo_game_assets_url}home_sweet_home/images/cinematic_inside_chalet.png`,
                        cinematic_cabin_1_image_link: `${demo_game_assets_url}home_sweet_home/images/cinematic_cabin_1.png`,
                        cinematic_cabin_2_image_link: `${demo_game_assets_url}home_sweet_home/images/cinematic_cabin_2.png`,
                        cinematic_cabin_3_image_link: `${demo_game_assets_url}home_sweet_home/images/cinematic_cabin_3.png`,
                    },
                },
                {
                    title: "Complete the playable scenes configuration",
                    slug: "complete-configuration",
                },
                {
                    title: "Game starting point",
                    slug: "game-starting-point",
                    image_steps: ["screen_starting_point.png"]
                },
                {
                    title: "Finalization of the interface customization",
                    slug: "finalization-interface-customization",
                    image_steps: [
                        "screen_choices_panel_settings.png",
                        "screen_settings_text_boxes_1.png",
                        "screen_settings_text_boxes_2.png"
                    ]
                },
                {
                    title: "Export the project",
                    slug: "export-project",
                    image_steps: [
                        "screen_export_project_1.png",
                        "screen_export_project_2.png",
                    ]
                },
                {
                    title: "Import a project",
                    slug: "import-project",
                    image_steps: [
                        "screen_import_project_1.png",
                        "screen_import_project_2.png",
                        "screen_import_project_3.png",
                    ]
                },
                {
                    title: "Test playing the game",
                    slug: "test-playing-game",
                    image_steps: [
                        "screen_test_play_1.png",
                        "screen_test_play_2.png"
                    ]
                }
            ]
        }),
        new Chapter({
            title: "Application functionalities reference",
            slug: "app-functionalities-reference",
            sections: [
                {
                    title: "Import / export a project",
                    slug: "import-export-project",
                    image_steps: ["screen_import_project_1.png",
                        "screen_import_project_2.png",
                        "screen_import_project_3.png",
                        "screen_export_project_1.png",
                        "screen_export_project_2.png",
                    ]
                },
                {
                    title: "Images",
                    slug: "images",
                    image_steps: [
                        "screen_add_animation_1.png",
                        "screen_add_animation_2.png",
                        "screen_add_animation_3.png",
                        "screen_add_animation_4.png",
                        "screen_add_animation_5.png",
                    ]
                },
                {
                    title: "Soundtracks",
                    slug: "soundtracks",
                    image_steps: ["screen_add_soundtrack.png"]
                },
                {
                    title: "Objects",
                    slug: "objects",
                    image_steps: [
                        "screen_add_remove_object_1.png",
                        "screen_add_remove_object_2.png",
                        "screen_add_remove_object_3.png",
                        "screen_add_remove_object_4.png",
                        "screen_add_remove_object_5.png",
                        "screen_add_remove_object_6.png"
                    ]
                },
                {
                    title: "Text boxes",
                    slug: "text-boxes",
                    image_steps: [
                        "screen_add_text_box_1.png",
                        "screen_add_text_box_2.png",
                        "screen_add_text_box_3.png",
                    ]
                },
                {
                    title: "Scenes",
                    slug: "scenes",
                    image_steps: [
                        "screen_scenes_1.png",
                        "screen_scenes_2.png",
                        "screen_scenes_3.png",
                        "screen_scenes_4.png",
                        "screen_scenes_5.png",
                        "screen_scenes_6.png",
                    ]
                },
                {
                    title: "General customization",
                    slug: "general-customization",
                    image_steps: ["screen_general_settings_2.png"]
                },
                {
                    title: "Choices panel customization",
                    slug: "choices-panel-customization",
                    image_steps: ["screen_choices_panel_settings.png"]
                },
                {
                    title: "Inventory panel customization",
                    slug: "inventory-panel-customization",
                    image_steps: [
                        "screen_inventory_settings_1.png",
                        "screen_inventory_settings_2.png",
                    ]
                },
                {
                    title: "Text boxes customization",
                    slug: "text-boxes-customization",
                    image_steps: [
                        "screen_settings_text_boxes_1.png",
                        "screen_settings_text_boxes_2.png"
                    ]
                }
            ]
        }),
        new Chapter({
            title: "The drawing tool",
            slug: "drawing-tool",
            image: "screen_open_drawing_tool.png",
            sections: [
                {
                    title: "The drawing tools",
                    slug: "drawing-tools",
                    image_steps: [
                        "screen_mtldt_drawing_tools_1.png",
                        "screen_mtldt_drawing_tools_2.png",
                        "screen_mtldt_drawing_tools_3.png",
                        "screen_mtldt_drawing_tools_4.png",
                        "screen_mtldt_drawing_tools_5.png"
                    ]
                },
                {
                    title: "Mode scene or object",
                    slug: "mode-scene-or-object",
                    image_steps: [
                        "screen_mtldt_modes_scene_object_1.png",
                        "screen_mtldt_modes_scene_object_2.png"
                    ],
                },
                {
                    title: "Image resolution",
                    slug: "image-resolution",
                    image_steps: [
                        "screen_mtldt_image_resolution_1.png",
                        "screen_mtldt_image_resolution_2.png"
                    ],
                },
                {
                    title: "Keyboard shortcuts",
                    slug: "keyboard-shortcuts"
                },
                {
                    title: "Animation mode",
                    slug: "animation-mode",
                    image_steps: ["screen_mtldt_animation_mode.png"]
                },
                {
                    title: "Handle animation frames",
                    slug: "handle-animation-frames",
                    image_steps: [
                        "screen_mtldt_frames_1.png",
                        "screen_mtldt_frames_2.png",
                        "screen_mtldt_frames_3.png",
                        "screen_mtldt_frames_4.png",
                    ],
                },
                {
                    title: "Onion skins",
                    slug: "onion-skins",
                    image_steps: [
                        "screen_mtldt_onion_skins_1.png",
                        "screen_mtldt_onion_skins_2.png",
                        "screen_mtldt_onion_skins_3.png",
                        "screen_mtldt_onion_skins_4.png",
                        "screen_mtldt_onion_skins_5.png",
                    ]
                },
                {
                    title: "Play the animation",
                    slug: "play-animation",
                    image_steps: [
                        "screen_mtldt_play_animation_1.png",
                        "screen_mtldt_play_animation_2.gif",
                    ]
                },
                {
                    title: "Files",
                    slug: "files",
                    image_steps: [
                        "screen_mtldt_files_1.png",
                        "screen_mtldt_files_2.png",
                        "screen_mtldt_files_3.png",
                        "screen_mtldt_files_4.png",
                    ],
                },
                {
                    title: "Exit the drawing tool",
                    slug: "exit-drawing-tool",
                    image_steps: [
                        "screen_mtldt_exit_1.png",
                        "screen_mtldt_exit_2.png"
                    ],
                }
            ]
        }),
        new Chapter({
            title: "Play, publish, update and delete games",
            slug: "crud-games",
            image: "screen_crud_games.png",
            sections: []
        })
    ],
}