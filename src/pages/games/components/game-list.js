"use strict";

const {
    fetch_game_list,
    fetch_game_search,
    fetch_publishing_license_link,
    fetch_one_game_overview
} = require("../xhr");
const GameListItem = require("./game-list-item");
const icon_play = require("../../../jsvg/icon_play");
const translator = require("ks-cheap-translator");
const NotifPopup = require("../../../notif-popup");
const GameView = require("./game-view");
const t = translator.trad.bind(translator);

/**
 * The component that displays a game list
 */
class GameList {
    constructor(params) {
        this.params = params;
        this.sort_modes = [
            {
                text: "Latest",
                value: "date_desc",
                selected: true,
            },
            {
                text: "Oldest",
                value: "date_asc",
            },
            {
                text: "A-Z",
                value: "name_asc",
            },
            {
                text: "Z-A",
                value: "name_desc",
            },
            {
                text: "Author",
                value: "author_asc",
            }
        ];

        this.state = {
            items: [],
            fetch_page: 1,
            disable_next_page: false,
            license_link: "",
            loading: false,
            initialized: false,
        };

        fetch_publishing_license_link()
            .then(link => {
                this.state.license_link = link;
            })
            .finally(this.refresh_license_info.bind(this));
    }

    /**
     * Initialize the first list of game to fetch. If a search param is present it will be used.
     * Default is latest games.
     */
    init() {
        if (!this.state.initialized) {
            const url_params = new URLSearchParams(window.location.search);
            if (url_params.has("query")) {
                const search_input = document.querySelector("input[type=search]");
                search_input.value = url_params.get("query");
                this.fetch_from_querystring(url_params);
            } else {
                this.fetch_list_data();
            }

            if (url_params.has("game_view")) {
                fetch_one_game_overview(url_params.get("game_view"))
                    .then(this.handle_show_game_view.bind(this))
                    .catch(err => {
                        const url = new URL(window.location);
                        url.searchParams.delete("game_view");
                        window.history.replaceState({}, "", url);
                        console.log(err);
                    });
            }
            this.state.initialized = true;
        }
    }

    /**
     * Shows the game list item in a modal window.
     * @param {GameListItem|Object} game 
     */
    handle_show_game_view(game) {
        const url = new URL(window.location);
        url.searchParams.set("game_view", game._id.$oid);
        window.history.replaceState({}, "", url);

        const save_data = {};
        this.handle_update_page_meta_from_game_view(game, save_data);

        new GameView({
            game,
            on_close: () => {
                url.searchParams.delete("game_view");
                window.history.replaceState({}, "", url);
                document.querySelector("html")
                    .replaceChild(save_data.head, document.querySelector("head"))
            },
        }).show();
    }

    /**
     * Updates the page meta tags in order to make this page state sharable.
     * @param {GameListItem|Object} game
     */
    handle_update_page_meta_from_game_view(game, save_data) {
        const page_head = document.querySelector("head");
        save_data.head = page_head.cloneNode(true);
        const page_title = "Mentalo - " + game.name;
        const page_image = game.publishing_overview.banner_image;

        page_head.querySelector("title").innerHTML = page_title;
        page_head.querySelector("meta[name='description']")
            .setAttribute("content", game.publishing_overview.description);
        page_head.querySelector("meta[property='og:description']")
            .setAttribute("content", game.publishing_overview.description);
        page_head.querySelector("meta[property='og:title']").setAttribute("content", page_title);
        page_head.querySelector("meta[property='og:image']").setAttribute("content", page_image);
        page_head.querySelector("meta[property='twitter:image']").setAttribute("content", page_image);
        page_head.querySelector("meta[property='og:url']").setAttribute("content", window.location);
    }

    /**
     * Sends a search games xhr from the given search params
     * @param {URLSearchParams} url_params 
     */
    fetch_from_querystring(url_params) {
        fetch_game_search(url_params)
            .then(results => {
                this.populate_game_list(results);
                this.refresh_list_items();
                this.refresh_pagination();
            })
            .catch(err => {
                console.log(err);
            });
    }

    /**
     * Populate the list of result from an xhr response results
     * @param {Array<Object>} items 
     * @param {Object} options options object can indicate if request is called from a results page change
     */
    populate_game_list(items, options = {}) {
        if (items.length === 0) {
            this.state.disable_next_page = true;
            if (options.from_page_change) {
                this.state.fetch_page = this.state.fetch_page > 1 ? this.state.fetch_page - 1 : 1;
            } else {
                this.state.items = []
            }
        } else {
            this.state.disable_next_page = false;
            this.state.items = items;
        }
    }

    /**
     * Sends an xhr to fetch a game list with given params (sort_by, page)
     * Populates and refresh the results list in case of success
     * @param {Object} options can indicate if request is called from a results page change
     */
    fetch_list_data(options) {
        fetch_game_list(this.state.fetch_page, this.sort_modes.find(mode => mode.selected).value)
            .then(items => {
                this.populate_game_list(items, options);
                this.refresh_list_items();
                this.refresh_pagination();
            })
            .catch(err => {
                console.log(err);
                (new NotifPopup({ error: true, message: t("Error, couldn't fetch games") })).pop()
            });
    }

    /**
     * Handle change from select input for results sorting mode.
     * @param {Event} e 
     */
    handle_change_sort_mode(e) {
        this.sort_modes.forEach(mode => {
            mode.selected = mode.value === e.target.value;
        });
        this.fetch_list_data();
    }

    /**
     * Handles click on the results paginaion buttons.
     * Fetches a new page of results
     * @param {Integer} next_or_prev 1 or -1
     */
    handle_change_page(next_or_prev) {
        switch (next_or_prev) {
            case 1:
                this.state.fetch_page += 1;
                break;
            case -1:
                this.state.fetch_page = this.state.fetch_page > 1 ? this.state.fetch_page - 1 : 1;
                break;
        }
        this.fetch_list_data({ from_page_change: true });
    }

    /**
     * Handles the submitting of a search query from the search bar
     * @param {Event} e 
     */
    handle_search(e) {
        e.preventDefault();
        const form_data = new FormData(e.target);
        const clean_query = form_data.get("query").trim();
        if (clean_query.length > 0 && clean_query.length < 3) {
            // This is also protected on server side.
            (new NotifPopup({
                error: true,
                message: t("Please type at least 3 characters to make a search query.")
            })).pop()
        } else if (clean_query.length > 0) {
            form_data.set("query", clean_query);

            const new_search_params = new URLSearchParams(form_data);
            const url = new URL(window.location);

            url.searchParams.set("query", clean_query);
            window.history.replaceState({}, "", url);

            fetch_game_search(new_search_params)
                .then(results => {
                    this.populate_game_list(results);
                    this.refresh_list_items();
                    this.refresh_pagination();
                })
                .catch(err => {
                    console.log(err);
                    (new NotifPopup({ error: true, message: t("Error fetching search results") })).pop();
                });
        }
    }

    /**
     * @returns {Object} object representation of the html to render for the sorting types select input.
     */
    render_sort_mode_select() {
        return {
            tag: "div",
            id: "game-list-select-sort-mode",
            contents: [
                { tag: "label", contents: t("Sort by") },
                {
                    tag: "select",
                    onchange: this.handle_change_sort_mode.bind(this),
                    contents: this.sort_modes.map(sort_mode => {
                        return {
                            tag: "option",
                            contents: t(sort_mode.text),
                            value: sort_mode.value,
                            selected: sort_mode.selected
                        }
                    })
                },
            ]
        };
    }

    /**
     * @returns {Object} object representation of the html to render for the pagination buttons
     */
    render_pagination() {
        return {
            tag: "div",
            id: "game-list-pagination",
            contents: [
                {
                    tag: "button",
                    style_rules: {
                        transform: "rotate(-180deg)"
                    },
                    contents: [{ ...icon_play }],
                    onclick: this.handle_change_page.bind(this, -1),
                    disabled: this.state.fetch_page === 1,
                    tooltip: t("Previous page")
                },
                {
                    tag: "button",
                    contents: [{ ...icon_play }],
                    onclick: this.handle_change_page.bind(this, 1),
                    disabled: this.state.disable_next_page,
                    tooltip: t("Next page")
                }
            ]
        }
    }

    /**
     * Refreshes the rendering of the pagination buttons
     */
    refresh_pagination() {
        obj2htm.subRender(this.render_pagination(), document.getElementById("game-list-pagination"), { mode: "replace" })
    }

    /**
     * @returns {Object} object representation of the html to render for the toolbar
     */
    render_toolbar() {
        return {
            tag: "div",
            id: "game-list-toolbar",
            contents: [
                this.render_sort_mode_select(),
                this.render_pagination(),
                {
                    tag: "form",
                    id: "game-list-search-form",
                    onsubmit: this.handle_search.bind(this),
                    contents: [
                        { tag: "input", type: "search", name: "query", placeholder: t("Search games by name") },
                        { tag: "input", type: "submit", value: "Go" }
                    ]
                },
                {
                    tag: "button",
                    id: "game-list-publish-btn",
                    contents: t("Publish"),
                    tooltip: t("Publish a new game or update one of those you already published."),
                    onclick: this.params.open_publish_modal
                }
            ]
        }
    }

    /**
     * Refreshes the rendering of the results list
     */
    refresh_list_items() {
        obj2htm.subRender(this.render_list_items(), document.getElementById("game-list-items"), { mode: "replace" })
    }

    /**
     * @returns {Object} object representation of the html to render for the results list
     */
    render_list_items() {
        return {
            tag: "ul",
            id: "game-list-items",
            contents: this.state.items.map(game => new GameListItem({ game, show_game_view: this.handle_show_game_view.bind(this) }).render())
        }
    }

    /**
     * Refreshes the rendering of the license info
     */
    refresh_license_info() {
        obj2htm.subRender(this.render_license_info(), document.getElementById("license-info"), { mode: "replace" })
    }

    /**
     * @returns {Object} object representation of the html to render for the license info
     */
    render_license_info() {
        const { license_link } = this.state;
        return license_link.length > 0 ? {
            tag: "p",
            id: "license-info",
            contents: t(`All games are published under Creative Commons license`, { license_link })
        } : { tag: "p", id: "license-info" }
    }

    /**
     * Refreshes the rendering tree from this component
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById("game-list"), { mode: "replace" })
    }

    /**
     * @returns {Object} object representation of the html to render for this component
     */
    render() {
        return {
            tag: "div",
            id: "game-list",
            on_render: this.init.bind(this),
            contents: [
                this.render_toolbar(),
                this.render_license_info(),
                this.render_list_items(),
            ]
        }
    }
}

module.exports = GameList;