"use strict";
const PublishGameDialog = require("./publish-game-dialog");

/**
 * This component is a container for the PublishGameDialog component.
 * It doesn't have a render method, it simply creates a modal container directly in the DOM
 * and calls the rendering of the PublishGameDialog instance inside of it.
 */
class PublishGameModal {
    constructor() {
        this.id = "publish-game-modal-container";
        this.container = document.createElement("div");
        this.container.id = this.id;
        this.escape_listener = this.escape_listener.bind(this);
    }

    /**
     * Creates a PublishGameDialog instance, inserts the modal element in 
     * the root of the app and calls the render() of the dialog window.
     */
    open() {
        this.dialog = new PublishGameDialog({ on_close: this.close.bind(this) });
        window.addEventListener("keydown", this.escape_listener);
        document.getElementsByTagName("main")[0].appendChild(this.container);
        obj2htm.subRender(this.dialog.render(), this.container);
        document.body.style.overflow = "hidden";
    }

    /**
     * Removes the modal container from the DOM.
     */
    close() {
        delete this.dialog;
        this.container.innerHTML = "";
        this.container.remove();
        window.removeEventListener("keydown", this.escape_listener);
        document.body.style.overflow = "visible";

    }

    /**
     * Event listener to handle when the Escape key is pressed.
     * @param {Event} e 
     */
    escape_listener(e) {
        if (e.key === "Escape") {
            this.close()
        }
    }
}

module.exports = PublishGameModal;