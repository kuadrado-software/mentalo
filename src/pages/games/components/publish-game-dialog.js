"use strict";
const translator = require("ks-cheap-translator");
const PublishGameFormSteps = require("./publish-game-form-steps");
const t = translator.trad.bind(translator);

/**
 * The component that displays the dialog window to publish update or delete games.
 */
class PublishGameDialog {
    constructor(params) {
        this.params = params;
        this.id = "publish-game-dialog";
        this.state = {
            mode: "publish"
        };
        this.form_steps = new PublishGameFormSteps({ on_close: this.on_close.bind(this), get_mode: () => this.state.mode });
    }

    /**
     * The callback being passed as argument to the PublishGameFormSteps child component.
     * Called if the closing of the window is requested from the child component.
     */
    on_close() {
        this.form_steps.clean_step_specific_listeners();
        this.params.on_close();
    }

    /**
     * Handle the clicks on the different tabs corresponding to the different forms
     * @param {String} mode "publish" "update" or "request_deletion"
     */
    handle_change_mode(mode) {
        this.state.mode = mode;
        this.form_steps.clean_step_specific_listeners();
        this.refresh();
    }

    /**
     * Refreshes the rendering tree from this component
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
    }

    /**
     * @returns {Object} object representation of the html to render for this component
     */
    render() {
        const { mode } = this.state;
        return {
            tag: "div",
            id: this.id,
            contents: [
                {
                    tag: "div",
                    id: "publish-game-topbar",
                    contents: [
                        {
                            tag: "button",
                            class: "publish-game-mode-tab " + (mode === "publish" ? "active" : ""),
                            contents: t("Publish"),
                            tooltip: t("Select this mode if want to publish a new game"),
                            onclick: this.handle_change_mode.bind(this, "publish"),
                        },
                        {
                            tag: "button",
                            class: "publish-game-mode-tab " + (mode === "update" ? "active" : ""),
                            contents: t("Update"),
                            tooltip: t("Select this mode if you want to publish a new version of a game you already published."),
                            onclick: this.handle_change_mode.bind(this, "update"),
                        },
                        {
                            tag: "button",
                            class: "publish-game-mode-tab " + (mode === "request_deletion" ? "active" : ""),
                            contents: t("Delete"),
                            tooltip: t("Select this mode if you want to delete a game you published."),
                            onclick: this.handle_change_mode.bind(this, "request_deletion"),
                        },
                        {
                            tag: "span",
                            id: "publish-game-dialog-close-icon",
                            contents: "x",
                            onclick: this.on_close.bind(this)
                        },
                    ],
                },
                this.form_steps.render()
            ]
        }
    }
}

module.exports = PublishGameDialog