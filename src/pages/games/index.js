"use strict";

const renderer = require("object-to-html-renderer");
const GamesPage = require("./games-page");

renderer.register("obj2htm");

/**
 * Sets the root component for the /games/ page
 */
obj2htm.setRenderCycleRoot(new GamesPage());
obj2htm.renderCycle();