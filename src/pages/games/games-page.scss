#games-page {
	height: 100vh;
	h1 {
		margin: 0;
	}
	color: $grey_4;

	#license-info {
		margin: 10px 20px 0;
		font-size: 10px;
		text-align: right;
	}

	#game-list-toolbar {
		display: flex;
		gap: 20px;
		margin: 0 20px;
		align-items: center;
		#game-list-select-sort-mode {
			display: flex;
			align-items: center;
			gap: 10px;
		}
		#game-list-pagination {
			display: flex;
			gap: 5px;
			button {
				border: none;
				border-radius: 100%;
				background-color: #0003;
				width: 35px;
				height: 35px;
				display: flex;
				justify-content: center;
				align-items: center;
				overflow: hidden;
				cursor: pointer;
				svg {
					&:first-child {
						left: 10%;
					}
					&:last-child {
						right: 10%;
					}
					fill: $grey_4;
					width: 70%;
					margin: 0 auto;
					position: relative;
				}
				&:hover {
					background-color: #0007;
					svg {
						fill: $grey_5;
					}
				}
				&:disabled {
					pointer-events: none;
					svg {
						fill: $grey_3;
					}
				}
			}
		}
		#game-list-search-form {
			display: flex;
			gap: 5px;
			height: 30px;
			margin-left: auto;
			input[type="search"] {
				border: none;
				padding: 0 8px;
				width: 300px;
				outline: none;
			}
			input[type="submit"] {
				background-color: #0003;
				border: none;
				cursor: pointer;
				color: $grey_4;
				font-weight: bold;
				width: 30px;
				&:hover {
					color: $grey_5;
					background-color: #0007;
				}
			}
		}

		#game-list-publish-btn {
			margin-left: auto;
			cursor: pointer;
			width: 150px;
			height: 40px;
			font-weight: bold;
			font-size: 20px;
			color: $grey_4;
			background-color: #0003;
			border: none;
			&:hover {
				color: $yellow;
				background-color: #0007;
			}
		}

		@media screen and (max-width: 850px) {
			display: grid;
			grid-template-columns: auto auto;
			justify-content: flex-start;
			#game-list-publish-btn {
				display: none;
			}
			#game-list-search-form {
				grid-column: 1 / span 2;
			}
		}
	}
	ul#game-list-items {
		margin: 0;
		padding: 20px;
		display: flex;
		flex-wrap: wrap;
		gap: 20px;
		list-style-type: none;
	}

	.game-view,
	li.game-list-item {
		display: grid;
		position: relative;
		&.game-list-item {
			width: 400px;
			grid-template-rows: 300px auto;
			grid-template-columns: auto 50px;
			background-color: #0003;
		}
		&.game-view {
			width: 800px;
			grid-template-rows: 500px auto;
			grid-template-columns: auto 50px;
			background-color: #222;
			padding: 40px;
			gap: 20px;
			@media screen and (max-width: 850px) {
				width: 90vw;
				grid-template-rows: 200px auto auto auto;
				gap: 5px;
				grid-template-columns: 1fr;
				padding: 10px;
				.game-info {
					grid-row: 2;
					grid-column: 1 / span 2;
				}
				.game-actions {
					grid-row: 3;
					grid-column: 1 / span 2;
					flex-direction: row;
				}
			}
		}
		.game-btn {
			cursor: pointer;
			border-radius: 100%;
			border: none;
			background-color: #0003;
			padding: 0;
			display: flex;
			justify-content: center;
			align-items: center;
			overflow: hidden;
			color: $grey_3;
			& * {
				color: $grey_3;
			}
			svg {
				fill: $grey_3;
				width: 50%;
			}
			&:hover {
				background-color: #0007;
				color: $grey_5;
				svg {
					fill: $grey_5;
				}
				& * {
					color: $grey_5;
				}
			}
		}
		.game-banner {
			display: flex;
			justify-content: center;
			align-items: center;
			overflow: hidden;
			position: relative;
			grid-column: 1 / span 2;
			img {
				position: absolute;
				height: 100%;
			}
		}

		.game-info {
			padding: 10px;
			display: flex;
			flex-direction: column;
			.game-title {
				padding: 10px 0;
				* {
					font-size: 20px;
				}
			}
			.game-details {
				display: flex;
				align-items: center;
				gap: 10px;
				.game-author {
					font-size: 12px;
					white-space: nowrap;
					overflow: hidden;
					text-overflow: ellipsis;
				}
				.game-date {
					font-size: 12px;
					margin-left: auto;
				}
				.game-version {
					font-size: 12px;
					color: $grey_5;
				}
				.game-btn {
					width: 26px;
					height: 26px;
					text-decoration: none;
					& > * {
						font-size: 18px;
						position: relative;
					}
					&:hover {
						& > * {
							color: $green;
						}
					}
				}
			}

			.game-list-item-description {
				color: $grey_5;
				padding: 10px;
			}
		}

		&.game-list-item {
			.game-banner {
				cursor: pointer;
				img {
					opacity: 0.8;
				}
				&:hover {
					img {
						opacity: 1;
					}
				}
			}
			.game-info {
				.game-title {
					cursor: pointer;
					&:hover {
						* {
							color: $light_green;
						}
					}
				}
			}
		}

		.game-actions {
			display: flex;
			flex-direction: column;
			justify-content: space-evenly;
			align-items: center;
			padding: 5px;
			gap: 5px;
			button.game-btn {
				width: 38px;
				height: 38px;
				svg {
					position: relative;
					&.icon-play {
						left: 7%;
					}
					&.icon-export {
						transform: rotate(-90deg);
						top: 7%;
					}
				}
				&:hover {
					svg {
						&.icon-play {
							fill: $green;
						}
						&.icon-pen {
							fill: $yellow;
						}
						&.icon-export {
							fill: $blue;
						}
					}
				}
			}
		}
	}
}

#publish-game-modal-container {
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	background-color: #0007;
	z-index: 10;
	display: flex;
	justify-content: center;
	align-items: center;
	#publish-game-dialog {
		background-color: $grey_2;
		width: 600px;
		height: 540px;
		position: relative;
		display: grid;
		grid-template-rows: auto 1fr;
		#publish-game-topbar {
			display: flex;
			height: 30px;
			.publish-game-mode-tab {
				border: none;
				flex: 1;
				cursor: pointer;
				background-color: $grey_1;
				color: $grey_3;
				font-weight: bold;
				font-size: 16px;
				border-width: 1px;
				border-style: solid;
				border-color: transparent;
				&.active {
					background-color: $grey_0;
					color: $grey_6;
					border-color: $green;
				}
			}
			#publish-game-dialog-close-icon {
				cursor: pointer;
				width: 30px;
				background-color: $grey_1;
				display: flex;
				justify-content: center;
				align-items: center;
				color: $grey_4;
				position: relative;
				border-radius: 100%;
				left: 10px;
				bottom: 10px;
				font-weight: bold;
				border: 1px solid transparent;
				&:hover {
					color: $yellow;
					background-color: $grey_0;
					border-color: $yellow;
				}
			}
		}

		#publish-game-form-steps {
			margin: 0 20px;
			display: flex;
			flex-direction: column;
			.steps-progress {
				display: flex;
				justify-content: space-evenly;
				margin: 10px;
				.steps-progress-step {
					display: flex;
					flex-direction: column;
					align-items: center;
					gap: 4px;
					position: relative;
					flex: 1;

					.step-progress-bullet {
						width: 30px;
						height: 30px;
						border-radius: 100%;
						display: flex;
						justify-content: center;
						align-items: center;
						background-color: $grey_5;
						font-weight: bold;
						color: $grey_1;
						border: 2px solid transparent;
						z-index: 2;
					}
					.step-progress-label {
						text-align: center;
						color: $grey_4;
					}

					&:not(:first-child) {
						&:not(.active):not(.complete):not(.error) {
							&::before {
								content: " ";
								display: block;
								border: 1px solid $grey_5;
								position: absolute;
								top: 13px;
								right: 60%;
								width: 80%;
							}
						}
						&.active:not(.complete),
						&.complete:not(.error) {
							&::before {
								content: " ";
								display: block;
								border: 1px solid $green;
								position: absolute;
								top: 13px;
								right: 60%;
								width: 80%;
							}
						}

						&.error {
							&::before {
								content: " ";
								display: block;
								border: 1px solid $red;
								position: absolute;
								top: 13px;
								right: 60%;
								width: 80%;
							}
						}
					}

					&.complete {
						.step-progress-bullet {
							border-color: $green;
							background-color: $grey_1;
							color: $green;
						}
					}
					&.active:not(.complete) {
						.step-progress-bullet {
							border-color: $yellow;
							background-color: $grey_1;
							color: $yellow;
						}
					}
					&.error {
						.step-progress-bullet {
							border-color: $red;
							background-color: $grey_1;
							color: $red;
						}
					}
				}
			}
			.step-header {
				display: flex;
				align-items: center;
				gap: 20px;
				h3 {
					color: $grey_5;
				}
				.step-number {
					background-color: $grey_4;
					width: 30px;
					height: 30px;
					border-radius: 100%;
					display: flex;
					align-items: center;
					justify-content: center;
					font-weight: bold;
					color: $grey_1;
					font-size: 20px;
				}
			}
			.step-contents {
				.step-info {
					color: $grey_6;
				}
				.load-game-file-btn {
					display: flex;
					justify-content: center;
					align-items: center;
					cursor: pointer;
					height: 70px;
					width: 70px;
					background-color: #0003;
					padding: 8px;
					border-radius: 100%;
					svg {
						width: 70%;
						height: 70%;
						fill: $grey_4;
					}
					&:hover {
						background-color: #0007;
						svg {
							fill: $grey_6;
						}
					}
				}
				.loaded-game-name {
					background-color: #0003;
					color: $green;
					padding: 10px;
				}

				.step-banner-overview {
					// Banners are displayed in a container with a 4:3 container.
					width: 266px;
					height: 200px;

					overflow: hidden;
					grid-column: 1 / span 2;
					display: flex;
					justify-content: center;
					align-items: center;
					position: relative;
					margin: 0 auto;
					background-color: #0007;
					img {
						position: absolute;
						height: 100%;
					}
				}

				&.final-step {
					display: flex;
					flex-direction: column;
					flex: 1;
					.step-info {
						margin: 5px 0;
					}
					.step-final-explain {
						text-align: justify;
						margin: 5px 0;
						font-size: 11px;
						color: $grey_5;
						strong {
							text-align: center;
							display: block;
							font-size: 14px;
						}
					}
					#publish-game-step-final-send-button,
					#delete-game-step-final-send-button {
						width: 40px;
						height: 40px;
						display: flex;
						justify-content: center;
						align-items: center;
						transform: rotate(180deg);
						cursor: pointer;
						border: none;
						background-color: #0003;
						overflow: hidden;
						svg {
							width: 80%;
						}
						&:disabled {
							pointer-events: none;
							svg {
								fill: $grey_3;
							}
						}
						&:not(:disabled) {
							svg {
								fill: $green;
							}
							&:hover {
								background-color: #0007;
							}
						}
					}
					#publish-game-final-step-status {
						position: relative;
						flex: 1;
						.status-icon {
							width: 40px;
							height: 40px;
							display: flex;
							justify-content: center;
							align-items: center;
							svg {
								width: 30px;
							}
						}
						.spin-loader {
							width: 30px;
							height: 30px;
						}
						.icon-check {
							fill: $green;
						}
						.icon-delete {
							fill: $red;
						}
						.if-email-not-received {
							display: flex;
							align-items: center;
							position: absolute;
							margin-left: -100px;
							top: 45px;
							color: $yellow;
							font-size: 11px;
							font-style: italic;
							button {
								font-size: 11px;
								font-style: italic;
								color: $yellow;
								font-weight: 500;
								text-decoration: underline;
								cursor: pointer;
								border: none;
								background: none;
								&:hover {
									color: lighten($yellow, 15%);
								}
							}
							#resend-email-status {
								padding-left: 10px;
								&.spin-loader {
									width: 15px;
									height: 15px;
									border-width: 1px 1px 0 0;
									border-color: $yellow;
								}
							}
						}
						.final-step-error-message {
							position: absolute;
							margin-left: -200px;
							top: 45px;
							color: $red;
						}
					}
				}
				#update-game-find-results {
					ul {
						margin: 0;
						padding: 0;
						height: 170px;
						overflow: auto;
						li {
							display: grid;
							grid-template-columns: 100px 300px auto;
							grid-template-rows: auto auto;
							padding: 10px;
							gap: 10px;
							background-color: #0003;
							border: 2px solid transparent;
							height: 100px;
							&.selected {
								border: 2px solid $green;
								background-color: #0007;
							}
							.img-container {
								grid-column: 1;
								grid-row: 1 / span 2;
								width: 100%;
								display: flex;
								justify-content: center;
								align-items: center;
								position: relative;
								overflow: hidden;
								img {
									position: absolute;
									height: 100%;
								}
							}

							*:not(img):not(input) {
								grid-column: 2;
							}
							.checkbox-container {
								display: flex;
								justify-content: center;
								align-items: center;
								grid-column: 3;
								grid-row: 1 / span 2;
							}

							strong {
								color: $green;
							}
						}
					}
				}

				.search-bar {
					display: flex;
					gap: 5px;
					input[type="search"] {
						flex: 1;
					}
					input[type="submit"] {
						background-color: #0003;
						font-weight: bold;
						color: $grey_4;
						font-weight: bold;
						border: none;
						padding: 5px;
						cursor: pointer;
						&:hover {
							background-color: #0007;
							color: $grey_5;
						}
					}
				}
				.note-give-credit {
					color: $yellow;
					grid-column: 1 / span 2;
					margin: 0;
					font-size: 10px;
				}
			}

			#steps-nav {
				margin-top: auto;
				display: flex;
				justify-content: space-between;
				button {
					border: 2px solid transparent;
					cursor: pointer;
					min-width: 100px;
					height: 40px;
					margin: 10px 0;
					background-color: #0003;
					font-weight: bold;
					font-size: 16px;
					&:disabled {
						visibility: hidden;
					}
					color: $grey_5;
					&:hover {
						background-color: #0007;
					}
					&:last-child:not(.close-btn) {
						border-color: $green;
						color: $green;
					}

					&:first-child:hover {
						border-color: $yellow;
						color: $yellow;
					}
					&.close-btn:hover {
						color: $grey_5;
					}
				}
			}
		}
	}
}
