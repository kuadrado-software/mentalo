"use strict";

const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * Reads a .mtl file and returns a Promise resolving with the project object data.
 * @param {File} file A .mtl file
 * @returns {Promise<Object, String>} Return the parsed JSON data or a text error
 */
function load_project_data(file) {
    return new Promise((resolve, reject) => {
        if (typeof FileReader !== "function") {
            reject(t("The File API is not supported by your browser."));
        }
        const file_reader = new FileReader();
        file_reader.onload = () => {
            const project_data = JSON.parse(file_reader.result);
            resolve(project_data);
        };
        file_reader.readAsText(file);
    });
}

module.exports = {
    load_project_data,
};